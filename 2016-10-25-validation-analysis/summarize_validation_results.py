#! /usr/bin/env python
import subprocess
import csv
import sys

gtex_to_srr = {}
def read_gtex_to_srr():
    map_file = "../dat/SRARunTable.txt"
    with open(map_file) as mfh:
        mfhcsv = csv.DictReader(mfh, delimiter = "\t")
        for line in mfhcsv:
            gtex_to_srr[line['Sample_Name_s']] =  line['Run_s']

read_gtex_to_srr()
mpileup_results = "validation_candidates.mpileup_op.tsv"

preferred = {}
control = {}
with open(mpileup_results) as mpfh:
    for line in mpfh:
        line = line.rstrip("\n")
        fields = line.split("\t")
        sample_type = fields[-1]
        locus = fields[0] + ":" + str(int(fields[1])) + "-" + fields[1]
        #locus = fields[0] + ":" + str(int(fields[1]) - 1) + "-" + fields[1]
        value = (fields[-2], fields[4], fields[5], float(fields[5])/(float(fields[4]) + float(fields[5])))
        if sample_type == "preferred":
            if locus not in preferred:
                preferred[locus] = []
            preferred[locus].append(value)
        elif sample_type == "control":
            if locus not in control:
                control[locus] = []
            control[locus].append(value)
    print("Number of control loci is ", len(control), file = sys.stderr)
    print("Number of preferred loci is ", len(preferred), file = sys.stderr)
    for locus in preferred:
        seen_in_control = False
        control_vafs = []
        #Identify Germline sites
        if locus in control:
            for control1 in control[locus]:
                control_vafs.append(control1[3])
                if control1[3] > 0:
                    seen_in_control = True
                    print(locus + "\t" + "germline_in_control")
                    break
        #Sites without a controls - don't identify somatic for these
        elif locus not in control:
            print(locus + "\t" + "no-control-sample")
        #Get at the somatic sites
        if locus in control and not seen_in_control:
            for pref1 in preferred[locus]:
                gtex_id_half = pref1[0]
                sample = "NA"
                gtex_id_full = "NA"
                for gtex_id in  gtex_to_srr:
                    if gtex_id_half in gtex_id:
                        sample = gtex_to_srr[gtex_id]
                        gtex_id_full = gtex_id
                        break
                if pref1[3] != 0:
                    #print(locus + "\t" + "\t".join([str(x) for x in pref1]) + "\t" + ",".join([str(x) for x in control_vafs]))
                    tabix = "tabix  ../2016-08-31-whole-genome-calls-filtering-2/all_calls_sorted.tsv.gz " + locus
                    output = subprocess.check_output(tabix, shell=True)
                    output = output.decode("utf-8")
                    if output == "":
                        print(locus + "\t" + "\t".join([str(x) for x in pref1]) + "\t" + sample + "\t" + gtex_id_full + "\tsomatic-filtered-out")
                    else:
                        print(output + locus + "\t" + "\t".join([str(x) for x in pref1]) + "\t" + sample + "\t" + gtex_id_full + "\tsomatic-filtered-in")
                    #print("tabix  ../2016-08-31-whole-genome-calls-filtering-2/all_calls_sorted_filtered_11.tsv.gz " + locus)
                if pref1[3] == 0:
                    print(locus + "\t" + "\t".join([str(x) for x in pref1]) + "\t" + sample + "\t" + gtex_id_full  + "\tnocall-in-preferred")
