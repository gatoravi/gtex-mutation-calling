This project tracks analysis related to somatic mutation calling
from GTEx samples.

Maintained by Avinash Ramu under the supervision of Don Conrad.


##List of directories
- src/ - Code shared amongst directories goes here.
- dat/ - Contains some data files, some of the important ones are
    - list of samples
    - Hashimoto sample data
    - Exome capture space
- bin/ - binaries/executable scripts go here.
- doc/ - references go here
    - Andrew's initial callset is here
- 2016-04-12-mpileup_random_sites
- 2016-04-14-check_end_bias_for_andrew_sites
- 2016-04-18-mpileup_random_sites_withVDB
- 2016-04-19-mpileup_all_sites_withVDB
- 2016-04-22-mpileup_all_sites_withVDB_withsampleinfo
    - Tissue heatmaps, nmutations per tissue
- 2016-04-29-whole-genome-estimate
    - Estimate for scaling the analysis up from cancer genes to the whole genome
- 2016-05-03-mpileup_all_sites_withVDB_withsampleinfo_withallTags
- 2016-05-09-SI_statistic_num_mutations
    - Plot the number of mutations in each tissue against the SI stat for that sample
- 2016-05-20-hashi-analysis - Analysis related to the hashimoto samples
- 2016-05-22-age_vs_nmut - Plot number of mutations in each individual
    - plot against number of tissues per case
    - plot n_mut normalized by n_tissues against age of individual
- 2016-05-26-whole-genome-run
    - Track the jobs/analysis scaled up to the whole genome.
- 2016-06-05-generate-priors-fixed
    - Generate priors for only the sites in the SeqCap Exome capture
- 2016-06-16-applymodel-twosubjects
    - Apply the model to two test subjects - GTEX-12KS4 and GTEX-12WSJ
- 2016-06-24-generate-priors-all
    - Experimental, Scripts for generating priors at all sites - not used for calling
- 2016-06-26-generate-priors-fixed-nonzeroalts
    - Experimental, use only samples/sites with non-zero alt readcounts for priors - not used for calling
- 2016-06-29-get-GTEX-12WSJ-exome-genotype-readcounts
    - Get readcounts at random sites from exome genotypes to compute FNR, FPR etc
- 2016-07-06-applymodel-allsamples
    - Repeat the calling done on the two test samples on all the samples
- 2016-08-18-whole-genome-calls-filtering
    - Filter the callset for all samples, whole genome

##Misc

Files at MGI can be accessed using https://gscweb.gsc.wustl.edu/gscmnt/gc2802/halllab/cchiang/gtex/rna-seq/4101/bam/SRR*.bam
