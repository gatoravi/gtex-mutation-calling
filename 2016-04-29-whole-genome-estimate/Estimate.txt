I wanted to estimate how much time/space would be needed to scale this analysis up to the whole exome from just
the cancer genes. We can use a tool called bam-readcount written in C++ to get the readcounts. This works in
principle the same way as the mpileup output piped to the Perl script that we used before.

I used this command to get readcounts for one sample on the exome space (this is defined by the capture BED file
from Nimblegen)

So how big is this capture region:
 bedtools merge -i SeqCap_EZ_Exome_v3_primary.bed | awk '{ sum += $3 - $2; } END { print sum }'
 63,564,965
Thats`s about 63Mbp of the genome and that sounds ok.


time (bam-readcount0.7  -f /gscmnt/gc2607/mardiswilsonlab/aramu/dat/hs37/all_sequences.fa -b 20 -q 20 -w 2 -l SeqCap_EZ_Exome_v3_primary.bed /gscmnt/gc2802/halllab/cchiang/gtex/rna-seq/4101/bam/SRR1383018.bam  | awk '$4 > 10' >  SRR1383018_counts_genes.txt)
real    152m41.202s
user    151m39.630s
sys     5m20.250s
Each sample takes about 2.5 hours to run, lets say a conservative estimate is 3 hours,
so that`s about 30,000 hours of compute in total. This time can be parallelized in some
way that`s feasible, for example running 50 samples in parallel this would take about 600
hours or 25 days.

Lets see how much disk space we will need:
If I only look at sites that have atleast 10 reads, this file has 20,512,207 lines - thats 20M sites.
6.8G 2016-04-29 00:58 SRR1383018_counts_genes.txt
453M 2016-04-29 09:11 SRR1383018_counts_genes.txt.gz
So after compression each sample takes up about 0.5 GB, for 10,000 samples we need about 5000GB or 5TB space.


bam-readcount
bam-readcount0.7  -f /gscmnt/gc2607/mardiswilsonlab/aramu/dat/hs37/all_sequences.fa -b 20 -q 20 -w 2 /gscmnt/gc2802/halllab/cchiang/gtex/rna-seq/4101/bam/SRR1347500.bam -l region.list
12      56666258        T       350
=:0:0.00:0.00:0.00:0:0:0.00:0.00:0.00:0:0.00:0.00:0.00
A:0:0.00:0.00:0.00:0:0:0.00:0.00:0.00:0:0.00:0.00:0.00
C:0:0.00:0.00:0.00:0:0:0.00:0.00:0.00:0:0.00:0.00:0.00
G:26:255.00:31.81:0.00:24:2:0.00:0.03:67.96:24:0.97:76.00:0.90
T:324:255.00:36.47:20.46:180:144:0.51:0.00:3.90:191:0.46:76.00:0.51
N:0:0.00:0.00:0.00:0:0:0.00:0.00:0.00:0:0.00:0.00:0.00

samtools mpileup  -f /gscmnt/gc2607/mardiswilsonlab/aramu/dat/hs37/all_sequences.fa -Q 20 -q 20 /gscmnt/gc2802/halllab/cchiang/gtex/rna-seq/4101/bam/SRR1347500.bam -r 12:56666258-56666258
12      56666258        T       358     <......,.,.,,,...........,,,....,,..,....,,,,,,,,,.....,,,,,,,,,,,,,,,,,,,......,,,.,,,,..,..........................,,,,,,,,,.....,..,,,,,,,,,,,,.,.,,,,,,,,,..................,,,,,,....,,....,,..,,...,,..,,,,,,,,,..,,,,,,,,.,,,..,....,,........,,......,,,,,,,,,,,,,,,,........,,,,.,..........,,.,........,...................^~.^~.^~G^~G^~G^~G^~G^~G^~G^~G^~G^~G^~G^~G^~G^~G^~G^~G^~G^~G^~.^~G^~G^~G^~G^~G^~G^~g^~,^~,^~,^~g^~,    I>>>>=>>=FCFFFHA77I=HJACGF:DDFEHHAIIGFIF#GHHHGHHHHGG@HCHFHDHHAHHA8HHHHFH:HC(/8HHEI4<DIJJIFJHHDBBBIHGFJDDGGHFFFHHDHEHFIG.G@IJJIDHFHJJHGDGJHJIIJJJIHG>HFIJBGG=IH.G?FHHIJHGJFHIFHIICCJIIJHHGGJHFJHHIJHGFHJIHHHEIIE;G;IGIJEFDI@IGHHH9H?@HHEDFBG?GHHIHHCJ,BFHJHHHHF@CJ?F;FD>7;FB?CHJFHJE:HEDAEJE/IH<FGIFHF;DFDAHBEFDDD#@BB+BBBCB@?CC=@BB@B??!!!!!!!!!!!!!!!!!!?!!!!!!!??#!?

#Newer samtools
 ~/src/samtools/samtools mpileup -f /gscmnt/gc2607/mardiswilsonlab/aramu/dat/hs37/all_sequences.fa -Q 20 -q 20 /gscmnt/gc2802/halllab/cchiang/gtex/rna-seq/4101/bam/SRR1347500.bam -r 12:56666258-56666258
12      56666258        T       304     <......,.,.,,,...........,,,....,,..,...,,,,,,,,,.....,,,,,,,,,,,,,,,,,,,....,,.,,,,..,..........................,,,,,,,,,.....,..,,,,,,,,,,.,.,,,,,,,,..................,,,,,,....,,....,,..,,...,,..,,,,,,,,..,,,,,,,.,,..,....,.......,,......,,,,,,,,,........,,...........,,...........................^~.^~.^~.^~,      If>>>=g>=FCFFFHA77I=HJACGF:DkFEHHAInGFpFGHHHGHHHHeG@HCHFHDHHAHHA8HHHHFH:HC8HHEIRDIJJIjJHHDBBBqfiFJeDGGHFFFkHDHEoFIGJG@IJJIDHFkJJfGDGJHJIIJJIG>HFJBGG=IHGG?FllIJHGiFHIFHIICCJIIJHHGGJHdJHHIJHGFHJIHHHEIIE;G;IGJEFDI@IGHH9?@HHEDFBGGHHIHHCJBFHJHJHHFJF;F>;?CHJFHJE:HDAJCIH<FGIFHF;DFAHBEFDDD@BBBBBCB@?CC=@BB@B????
