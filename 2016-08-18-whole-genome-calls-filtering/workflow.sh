rm -f all_calls_sorted_filtered*
#Filter on variant supporting read-count
zcat < ~/Dropbox/GTEx_mutation_calling/2016-07-10-callset-all/all_calls_sorted.tsv.gz | awk '$6 > 5' > all_calls_sorted_filtered_1.tsv
#Filter on background VAF - Disable for now. Causes a huge drop in number of calls from 67M to 1.6M
#awk '$14/($14 + $13) < 0.001'  all_calls_sorted_filtered_1.tsv > all_calls_sorted_filtered_2.tsv
ln -s all_calls_sorted_filtered_1.tsv all_calls_sorted_filtered_2.tsv
#Filter on total readcount
awk '$5 + $6 > 20' all_calls_sorted_filtered_2.tsv > all_calls_sorted_filtered_3.tsv
#Convert 1-based to BED format
awk '{ printf($1"\t"$2-1"\t"$2); for (i = 3; i <= NF; i++) { printf("\t"$i); } printf("\n"); }'  all_calls_sorted_filtered_3.tsv > all_calls_sorted_filtered_3.bed
#Identify sites that don't overlap with exome SNP calls
bedtools intersect -a all_calls_sorted_filtered_3.bed -b ~/Dropbox/GTEx_mutation_calling/2016-08-19-Exome-genotype-calls/gtex_variants.avinput -wa -v > all_calls_sorted_filtered_4.tsv

zcat < /Users/aramu/Dropbox/GTEx_mutation_calling/2016-07-10-callset-all/all_calls_sorted.tsv.gz | wc -l
wc -l all_calls_sorted_filtered*

#Filter out the calls shared across tissues of an individual. This filtering is done per individual.
gtexmc filter-shared-across-samples all_calls_sorted_filtered_4.tsv ../dat/SRARunTable.txt 2>/dev/null | vcf-sort > all_calls_sorted_filtered_5.tsv

#Add tissue,subject information to bcall results
gtexmc add-tissue-subject-to-calls all_calls_sorted_filtered_5.tsv ../dat/SRARunTable.txt | vcf-sort > all_calls_sorted_filtered_6.tsv

#Get sample-specific call summaries
gtexmc nmutations-sample-bcall all_calls_sorted_filtered_6.tsv ../dat/SRARunTable.txt ../dat/Tuuli_SI/GTEx_Analysis_v6_ASE.exome.chrX_stats.zero.txt ../dat/phenotype_data/phs000424.v6.pht002742.v6.p1.c1.GTEx_Subject_Phenotypes.GRU.txt  > sample_summary.tsv

#Get subject-specific call summaries
gtexmc nmutations-subject-bcall all_calls_sorted_filtered_6.tsv ../dat/SRARunTable.txt ../dat/phenotype_data/phs000424.v6.pht002742.v6.p1.c1.GTEx_Subject_Phenotypes.GRU.txt 2>/dev/null > subject_summary.tsv

#Get tissue-specific call summaries
gtexmc nmutations-tissue-bcall all_calls_sorted_filtered_6.tsv ../dat/tissue_summary.txt  > tissue_summary.tsv

#Create a matrix of mutations shared between tissues
gtexmc parse-tissue-sample-bcall all_calls_sorted_filtered_6.tsv > tissue_matrix.tsv

#Get the number of samples for each tissue
python ../src/count_tissue_nsamples.py > ../dat/tissue_summary.txt
