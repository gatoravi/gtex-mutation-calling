#cat test_ins_mpileup.txt | ./mpileup2readcounts
#cat /gscmnt/gc2607/mardiswilsonlab/aramu/test/gtex/2016-04-29-whole-genome-estimate/SRR1068808_mpileup.txt | ./mpileup2readcounts
#head /gscmnt/gc2607/mardiswilsonlab/aramu/test/gtex/2016-04-29-whole-genome-estimate/SRR1068808_mpileup.txt | ./mpileup2readcounts

#This sample has 562 putative somatic mutations
rm -f SRR1479328_readcounts.txt; ~/src/samtools/samtools mpileup -q 20 -Q 20 -f /gscmnt/gc2607/mardiswilsonlab/aramu/dat/hs37/all_sequences.fa /gscmnt/gc2802/halllab/cchiang/gtex/rna-seq/4101/bam/SRR1479328.bam | ./mpileup2readcounts | awk '{ if(NR==1 || $5 + $6 > 10) print }' >  SRR1479328_readcounts.txt
rm -f SRR1479328_readcounts.txt.gz && bgzip SRR1479328_readcounts.txt && tabix -b 2 -e 2 -S 1 SRR1479328_readcounts.txt.gz
