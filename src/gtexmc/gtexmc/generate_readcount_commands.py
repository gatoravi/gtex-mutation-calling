#! /usr/bin/env
"""
Generate readcount commands
"""

def usage():
    print("gtexmc generate-readcountcommands sample_list.txt")
    return 0

fasta = "/gscmnt/gc2607/mardiswilsonlab/aramu/dat/hs37/Homo_sapiens_assembly19.fasta"
samtools = "~/src/samtools/samtools"
mpileup2readcounts = "../bin/mpileup2readcounts"
awk = "awk '{ if(NR==1 || \$5 + \$6 > 10) print }'"
output_dir = "/gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27"
bsub = "bsub"
log_dir = "./log"
bjgroup = " -g /aramu/gtexmc "
bgzip = "bgzip"

def tabix(gz):
    return "tabix -b 2 -e 2 -S 1 " + gz

def quotify(text):
    return "\"" + text + "\""

def bam(sample):
    return "/gscmnt/gc2802/halllab/cchiang/gtex/rna-seq/4101/bam/" + sample  + ".bam"

def samtools_params(fasta, bam):
    return samtools + " mpileup -q 20 -Q 20 -f " + fasta + " " + bam

def output(sample):
    return output_dir + "/" + sample + "_readcounts.txt.gz"

def log(sample):
    return " -oo " + log_dir + "/" + sample + ".log"

def construct_mpileup_command(sample):
    commands = " | ".join([samtools_params(fasta, bam(sample)), mpileup2readcounts, awk, bgzip])
    commands =  " > ".join([commands, output(sample)])
    commands = " ; ".join([commands, tabix(output(sample))])
    return commands

def generate_readcount_commands(argv):
    if len(argv) < 2:
        return usage()
    sample_list_file = argv[1]
    with open(sample_list_file) as sample_list_fh:
        for line in sample_list_fh:
            sample = line.rstrip("\n")
            mp_command = construct_mpileup_command(sample)
            bsub_command = bsub + log(sample) + bjgroup + quotify(mp_command)
            print(bsub_command)
