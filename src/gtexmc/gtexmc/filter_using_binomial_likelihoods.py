'''
    filter_using_binomial_likelihoods.py
    Filter the sites that look like het or hom-alt across all sites

    Copyright: Avinash Ramu <aramu@genome.wustl.edu>

    Author: Avinash Ramu <aramu@genome.wustl.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
'''

import sys
from math import log

def usage():
    print("gtexmc filter-using-binomial-likelihoods" \
          "bcall_results.tsv")

def binomial_llr(ref_count, alt_count, theta):
    "Inspired by Dave's binomial filter"
    "theta is the prob of seeing a variant read"
    """
    > (289 * log(0.001) + 429814 * log(1 - 0.001)) / (289 * log(0.313) + 429814 * log(1 - 0.313))
    [1] 0.01500567
    """
    error_rate = 0.01
    theta_prime = 1 - theta
    if theta == 0:
        theta = error_rate
    if theta_prime == 0:
        theta_prime = error_rate
    alt_lik = float(ref_count) * log(theta_prime) + float(alt_count) * log(theta) #alternate model likelihood
    ref_lik = float(ref_count) * log(1 - error_rate) + float(alt_count) * log(error_rate) #hom-ref likelihood
    #print (ref_lik, alt_lik, ref_lik - alt_lik)
    return (ref_lik - alt_lik) #How likely is hom-ref compared to the alternate model

def filter_bcall_line(line):
    "Return True if LLR > 10"
    error_rate = 0.001
    fields = line.split("\t")
    total_ref_count = int(fields[12])
    total_alt_count = int(fields[13])
    #total_vaf = float(total_alt_count)/(float(total_ref_count) + float(total_alt_count))
    altalt_vs_refref = binomial_llr(total_ref_count, total_alt_count, 1 - error_rate)
    refalt_vs_refref = binomial_llr(total_ref_count, total_alt_count, 0.5)
    if altalt_vs_refref > 10 and refalt_vs_refref > 10: #ref-ref is more likely than hom-alt and ref-alt
        return True

def filter_bcall_file(bcall_results_file):
    with open(bcall_results_file) as brf_fh:
        for line in brf_fh:
            line = line.rstrip("\n")
            if line.startswith("chr"): #header
                continue
            if filter_bcall_line(line):#True implies the prior indicates a hom-ref in all other sites
                print(line)

def filter_using_binomial_likelihoods(argv):
    "Filter the sites that look like hom-alt or het given the prior"
    if len(argv) != 2:
        return usage()
    bcall_results_file = argv[1]
    filter_bcall_file(bcall_results_file)
