'''
    subject_tissues.py
    Print the samples for each subject

    Copyright: Avinash Ramu <aramu@genome.wustl.edu>

    Author: Avinash Ramu <aramu@genome.wustl.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
'''
from .sra_runs import SRAMap

def usage():
    print("gtexmc subject-to-samples " \
        "../../../dat/SRARunTable.txt",
        "../../../dat/samplelist.txt")

def subject_to_samples(argv):
    "Print the samples for each subject"
    if len(argv) != 3:
        return usage()
    sra_run_file = argv[1]
    sequenced_samples = argv[2]
    sra1 = SRAMap(sra_run_file)
    sra1.read_sequenced_samples(sequenced_samples)
    sra1.read_srr_to_gtex_map()
    for subject, samples in sra1.subject_to_SRR_map.items():
        print (subject, len(samples), "\t".join(samples), sep = "\t")
