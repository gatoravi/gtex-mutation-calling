'''
    filter_rnaediting.py
    Filter putative RNA editing sites. If the transcribed strand
    is '+' and the change is A->G or if '-' and 'T'->'C' filter
    out.

    Copyright: Avinash Ramu <aramu@genome.wustl.edu>

    Author: Avinash Ramu <aramu@genome.wustl.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
'''
import sys

def usage():
    print("gtexmc filter-rnaediting " \
          "bcalls-calls.tsv " \
          "calls-annovar-variant-function.tsv " \
          "refseq-genes-strands.tsv")

#Key is gene, value is transcription strand
genes_strands = {}
#Key is locus, value is set of individuals with the call
#filter out if more than one individual have this putative
#RNA editing site
rnaediting = {}

def read_refseq_gs(refseq_gs):
    "Read the strand for each gene"
    with open(refseq_gs) as rfh:
        for line in rfh:
            line = line.rstrip("\n")
            fields = line.split("\t")
            gene = fields[0]
            strand = fields[1]
            genes_strands[gene] = strand

def store_editing_calls(calls_annotation):
    "Does this call look like RNA editing?"
    with open(calls_annotation) as cafh:
        for line in cafh:
            fields = line.split("\t")
            if fields[0] != "intergenic" and "upstream" not in fields[0] and "downstream" not in fields[0]:
                geneinfo = fields[1]
                chr = fields[2]
                pos = fields[4]
                ref = fields[5]
                alt = fields[6]
                indiv = fields[7]
                locus = chr + ":" + pos
                genesinfo_fields = geneinfo.split("(")
                genes = genesinfo_fields[0]
                for genes2 in genes.split(","):
                    for gene in genes2.split(";"):
                        try:
                            strand = genes_strands[gene]
                            if (strand == "+" and ref == "A" and alt == "G") or \
                               (strand == "-" and ref == "T" and alt == "C"):
                                   if locus not in rnaediting:
                                       rnaediting[locus] = set()
                                   #Add this individual to the rna-editing list
                                   rnaediting[locus].add(indiv)
                        except(KeyError):
                            print("unable to find strand " + line)
                            sys.exit(1)

def filter_calls(bcall_calls):
    "Print calls which don't look like rna-editing in more than one indiv"
    all_lines = []
    filtered = {}
    with open(bcall_calls) as bcfh:
        for line in bcfh:
            line = line.rstrip("\n")
            all_lines.append(line)
            fields = line.split("\t")
            locus = fields[0] + ":" + fields[2]
            if locus in rnaediting:
                indiv = rnaediting[locus]
                if len(indiv) > 1:
                    filtered[locus] = 1
        for line in all_lines:
            locus = fields[0] + ":" + fields[2]
            if locus not in filtered:
                print(line)

def filter_rnaediting(argv):
    "Action starts here!"
    if len(argv) < 4:
        return usage()
    calls = argv[1]
    calls_annotation = argv[2]
    refseq_gs = argv[3]
    read_refseq_gs(refseq_gs)
    store_editing_calls(calls_annotation)
    filter_calls(calls)
