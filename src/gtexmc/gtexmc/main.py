import sys
from .nmutations_sample import nmutations_sample_vcf
from .nmutations_sample import nmutations_sample_bcall
from .nmutations_subject import nmutations_subject_vcf
from .nmutations_subject import nmutations_subject_bcall
from .nmutations_hashi import nmutations_hashi
from .hashi_match_samples import hashi_match_samples
from .generate_readcount_commands import generate_readcount_commands
from .subject_to_samples import subject_to_samples
from .filter_shared_across_samples import filter_shared_across_samples
from .filter_using_binomial_likelihoods import filter_using_binomial_likelihoods
from .add_tissue_subject_to_calls import add_tissue_subject_to_calls
from .parse_tissue_sample_bcall import parse_tissue_sample_bcall
from .nmutations_tissue import nmutations_tissue_bcall
from .filter_individual_cumulative_vaf import filter_individual_cumulative_vaf
from .filter_individual_cumulative_vaf_binomialtest import filter_individual_cumulative_vaf_binomialtest
from .genotyper import genotyper
from .filter_max_variant_alleles import filter_max_variant_alleles
from .filter_using_vdb_and_strands import filter_using_vdb_and_strands
from .filter_flagged_samples import filter_flagged_samples
from .filter_rnaediting import filter_rnaediting

def usage():
    print("gtexmc")
    print("\nSubcommands:\n")
    print("\tadd-tissue-subject-to-calls - " \
          "Use the SRR id for each call to infer the tissue and subject this call belongs to." \
          "\n\t\t\t\tAssumes the last column of the calls file is the run_id")
    print("\tsubject-to-samples - " \
          "For each individual get the SRA run ID of the tissue samples.")
    print("\tnmutations-sample-vcf - " \
          "Summarize the number of mutations per sample along with SI metric. Input is in VCF")
    print("\tnmutations-sample-bcall - " \
          "Summarize the number of mutations per sample along with SI metric. Input is in bcall.")
    print("\thashi-match-samples - " \
          "Identify Hashimoto samples using phenotypes like BMI etc")
    print("\tnmutations-subject-vcf - " \
          "Compute total number of mutations in each subject. The calls are in the VCF format.")
    print("\tnmutations-subject-bcall - " \
          "Compute total number of mutations in each subject. The calls are in bcall format.")
    print("\tnmutations-tissue-bcall - " \
          "Compute total number of mutations in each tissue. The calls are in bcall format.")
    print("\tnmutations-hashi-samples - " \
          "Compute total number of mutations in hashi samples.")
    print("\tgenerate-readcount-commands - " \
          "Get the BASH commands for generating readcounts on samples.")
    print("\tfilter-shared-across-samples - " \
          "Filter the mutations shared across tissues of the same individual, likely germline")
    print("\tfilter-using-binomial-likelihoods - " \
          "Filter sites that don't look like hom-ref across rest of the sites.")
    print("\tfilter-individual-cumulative-vaf - " \
          "Filter sites based on cumulative VAF in an individual.")
    print("\tfilter-individual-cumulative-vaf-binomialtest - " \
          "Filter sites based on cumulative VAF in an individual. Filter statistically instead of hard cutoff.")
    print("\tparse-tissue-sample-bcall - " \
          "Create a matrix of nmutations shared across tissues and " \
          "individuals. The input is in bcall format")
    print("\tgenotyper - " \
          "Figure out the genotype at bcall calls")
    print("\tfilter-max-variant-alleles - " \
          "Filter out sites which have read support for more than two alleles")
    print("\tfilter-using-vdb-and-strands - " \
          "Filter out sites with a VDB cutoff. This is done on a per-site basis. If min(VDB) at a site < cutoff, filter out.")
    print("\tfilter-flagged-samples - " \
            "Remove calls that have been flagged by the GTEx consortium")
    print("\tfilter-rnaediting - " \
            "Filter calls that look like putative rna-editing sites")


def main():
    "Execution starts here"
    if len(sys.argv) < 2:
        return usage()
    elif sys.argv[1] == "add-tissue-subject-to-calls":
        add_tissue_subject_to_calls(sys.argv[1:])
    elif sys.argv[1] == "hashi-match-samples":
        hashi_match_samples(sys.argv[1:])
    elif sys.argv[1] == "nmutations-sample-vcf":
        nmutations_sample_vcf(sys.argv[1:])
    elif sys.argv[1] == "nmutations-sample-bcall":
        nmutations_sample_bcall(sys.argv[1:])
    elif sys.argv[1] == "nmutations-hashi-samples":
        nmutations_hashi(sys.argv[1:])
    elif sys.argv[1] == "nmutations-subject-vcf":
        nmutations_subject_vcf(sys.argv[1:])
    elif sys.argv[1] == "nmutations-subject-bcall":
        nmutations_subject_bcall(sys.argv[1:])
    elif sys.argv[1] == "nmutations-tissue-bcall":
        nmutations_tissue_bcall(sys.argv[1:])
    elif sys.argv[1] == "generate-readcount-commands":
        generate_readcount_commands(sys.argv[1:])
    elif sys.argv[1] == "subject-to-samples":
        subject_to_samples(sys.argv[1:])
    elif sys.argv[1] == "filter-shared-across-samples":
        filter_shared_across_samples(sys.argv[1:])
    elif sys.argv[1] == "filter-using-binomial-likelihoods":
        filter_using_binomial_likelihoods(sys.argv[1:])
    elif sys.argv[1] == "parse-tissue-sample-bcall":
        parse_tissue_sample_bcall(sys.argv[1:])
    elif sys.argv[1] == "filter-individual-cumulative-vaf":
        filter_individual_cumulative_vaf(sys.argv[1:])
    elif sys.argv[1] == "filter-individual-cumulative-vaf-binomialtest":
        filter_individual_cumulative_vaf_binomialtest(sys.argv[1:])
    elif sys.argv[1] == "genotyper":
        genotyper(sys.argv[1:])
    elif sys.argv[1] == "filter-max-variant-alleles":
        filter_max_variant_alleles(sys.argv[1:])
    elif sys.argv[1] == "filter-using-vdb-and-strands":
        filter_using_vdb_and_strands(sys.argv[1:])
    elif sys.argv[1] == "filter-flagged-samples":
        filter_flagged_samples(sys.argv[1:])
    elif sys.argv[1] == "filter-rnaediting":
        filter_rnaediting(sys.argv[1:])
    else:
        raise ValueError("Unknown sub-command " + sys.argv[1])
