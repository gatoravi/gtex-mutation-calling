'''
    sra_runs.py
    Deal with SRA GTEx sample maps

    Copyright: Avinash Ramu <aramu@genome.wustl.edu>

    Author: Avinash Ramu <aramu@genome.wustl.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
'''

import csv
import sys

class SRAMap:
    def __init__(self, sra_run_file1, gtex_pheno1 = None):
        self.sra_run_file = sra_run_file1
        self.gtex_pheno = gtex_pheno1
        self.SRR_to_samplename_map = {}
        self.SRR_to_subjectid = {}
        self.SRR_to_tissue = {}
        self.subject_to_SRR_map = {} #Jump from subject to list of samples
        self.gtex_to_SRR_map = {}
        self.gtex_to_age = {}
        self.SRR_to_sex = {} #Store if male or female, key is SRR id
        self.sequenced_samples = {} #These are the samples for which RNAseq data is available

    def read_sequenced_samples(self, sequenced_samples):
        "Read ../dat/samplelist.txt"
        with open(sequenced_samples) as ss_fh:
            for sample in ss_fh:
                sample = sample.rstrip("\n")
                self.sequenced_samples[sample] = 1

    def read_srr_to_gtex_map(self):
        "Read ../dat/SRARunTable.txt"
        sys.stderr.write(str(len(self.sequenced_samples)) + " samples have been sequenced.")
        with open(self.sra_run_file) as sra_run_fh:
            for row in csv.DictReader(sra_run_fh, delimiter = "\t"):
                run_s = row['Run_s'] #This is of the form SRR*
                subject_id = row['submitted_subject_id_s']
                sample_name = row['Sample_Name_s'] #This is of the form GTEx* - in Tuuli's file
                sex = row['sex_s'] #Sex
                tissue = row['body_site_s'] #tissue
                self.SRR_to_samplename_map[run_s] = sample_name
                self.SRR_to_subjectid[run_s] = subject_id
                self.SRR_to_tissue[run_s] = tissue
                self.gtex_to_SRR_map[sample_name] = run_s
                self.SRR_to_sex[run_s] = sex
                if subject_id not in self.subject_to_SRR_map:
                    self.subject_to_SRR_map[subject_id] = []
                if len(self.sequenced_samples) == 0 or run_s in self.sequenced_samples:
                    self.subject_to_SRR_map[subject_id].append(run_s)

    def read_subjectid_to_age_map(self):
        "Read phs000424.v6.pht002742.v6.p1.c1.GTEx_Subject_Phenotypes.GRU.txt"
        with open(self.gtex_pheno) as gtex_pheno_fh:
            for row in csv.DictReader(gtex_pheno_fh, delimiter = "\t"):
                subject_id = row['SUBJID']
                age = row['AGE']
                self.gtex_to_age[subject_id] = age
