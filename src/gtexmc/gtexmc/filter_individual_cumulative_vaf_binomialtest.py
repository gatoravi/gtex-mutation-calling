'''
    filter_individual_cumulative_vaf.py
    Filter the variants using the cumulative VAF across
    all tissues for an individual at a locus. Use a binomial
    test to reject germline sites at a locus.

    Two tests:
        T1:
            Null - VAF is 0.5
            Alternate - VAF < 0.5
            Keep the sites where the null is rejected.

    Copyright: Avinash Ramu <aramu@genome.wustl.edu>

    Author: Avinash Ramu <aramu@genome.wustl.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
'''
from .bcall import bcallMap
import sys
import scipy
import scipy.stats
import numpy as np
import statsmodels.sandbox.stats.multicomp

# awk '{ print $1":"$2 }' all_calls_sorted_filtered_3.tsv  | sort -u | wc -l
# 653741
#Adjust using Bonferroni corrected p-values
#Number of tests is  1558529
#pval_cutoff_hom = 0.05/653741
pval_cutoff_het = 0.0471716 #Determined with 5% FDR, look at gtex branch of FDR repo, also the binary gtex-bh-fdr in the bin/ folder

#Store all the p-values from the tests here, could use this for FDR
#all_pvals = np.array([])
all_pvals = []
ntest = 0

def usage():
    print("gtexmc filter-individual-cumulative-vaf-binomialtest " \
          "bcall_results.tsv " \
          "SRARunTable.txt ")
    print("Make sure that the 6th and 7th columns of the bcall file are ref and alt counts.")

def omit_site(ref_count, alt_count):
    "returns True if we reject this site"
    #cipy.stats.binom_test(alt_count, ref_count + alt_count, 1.0, alternative='less')2 = scipy.stats.binom_test(alt_count, ref_count + alt_count, 1.0, alternative='less')
    #if p2 > pval_cutoff_hom: #Looks hom
        #print("hom", alt_count, ref_count + alt_count, p2, file = sys.stderr)
    #    return True
    global all_pvals
    #all_pvals = np.append(all_pvals, p1)
    global ntest
    p1 = scipy.stats.binom_test(alt_count, ref_count + alt_count, 0.47, alternative='less')
    all_pvals.append(p1)
    ntest += 1
    if p1 > pval_cutoff_het: #Fail to reject the null, looks like het
        #print("het", alt_count, ref_count + alt_count, p1, file = sys.stderr)
        return True
    return False

def write_all_pvals():
    global all_pvals
    print("Writing p-values", file = sys.stderr)
    with open("all_pvals.tsv", "w") as apfh:
        for pval in all_pvals:
            apfh.write(str(pval) + "\n")

def filter_individual_cumulative_vaf_binomialtest(argv):
    "Filter the mutations shared across tissues of the same individual, likely germline"
    if len(argv) != 3:
        return usage()
    bcall_results_file = argv[1]
    sra_run_file = argv[2]
    print("Before reading calls!", file = sys.stderr)
    bm1 = bcallMap(bcall_results_file)
    bm1.read_calls(sra_run_file)
    print("p-value cutoff het is: ", pval_cutoff_het, file = sys.stderr)
    #print("p-value cutoff hom is: ", pval_cutoff_hom, file = sys.stderr)
    for locus, all_calls in bm1.calls.items():
        #Only print calls that are below the VAF threshold in all indiv"
        binomial_reject = False
        for subject in bm1.individual_cumulative_ref[locus]:
            ref_count = bm1.individual_cumulative_ref[locus][subject]
            alt_count = bm1.individual_cumulative_alt[locus][subject]
            n_samples = bm1.individual_nsamples[locus][subject] #Number of samples in this individual containing this call
            #if n_samples >1:
            if omit_site(ref_count, alt_count): #Check that this is in multiple tissues of same individual
                binomial_reject = True
                break
        if not binomial_reject: #Doesn't look germline in any subject
            for call in all_calls: #Print this locus' call in all subjects
                print(call)
            #Look at individual level VAFs to check if filtering worked.
            for subject in bm1.individual_cumulative_ref[locus]:
                ref_count = bm1.individual_cumulative_ref[locus][subject]
                alt_count = bm1.individual_cumulative_alt[locus][subject]
                print(locus, subject, ref_count, alt_count, alt_count/(ref_count + alt_count), n_samples, "cumul-stats-in", file = sys.stderr)
        else: #print the filtered out calls to STDERR
            for call in all_calls:
                print(call, "FILTERED_OUT", file = sys.stderr)
            #Look at individual level VAFs to check if filtering worked.
            for subject in bm1.individual_cumulative_ref[locus]:
                ref_count = bm1.individual_cumulative_ref[locus][subject]
                alt_count = bm1.individual_cumulative_alt[locus][subject]
                print(locus, subject, ref_count, alt_count, alt_count/(ref_count + alt_count), n_samples, "cumul-stats-out", file = sys.stderr)
    print("Number of tests is ", len(all_pvals), file = sys.stderr)
    write_all_pvals()
    #print("Size of p-values array is " + str(all_pvals.size))
    #all_pvals_adjusted = statsmodels.sandbox.stats.multicomp.fdrcorrection0(all_pvals)
    #print(all_pvals_adjusted[1])
