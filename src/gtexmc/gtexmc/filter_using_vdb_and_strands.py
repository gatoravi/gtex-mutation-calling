'''
    filter_using_vdb.py
    Filter the variants using the variant distance bias metric.

    Copyright: Avinash Ramu <aramu@genome.wustl.edu>

    Author: Avinash Ramu <aramu@genome.wustl.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
'''
from .bcall import bcallMap
import sys
import csv

calls_minvdb = {}
calls_minmqsb = {}
calls_robs = {} #Does the call have reads on both strands supporting the variant allele, 0 for no 1 for yes

def usage():
    print("gtexmc filter-using-vdb-and-strands " \
          "vdb_metrics.tsv " \
          "all-calls.tsv " \
          "VDB_cutoff")
    print("The VDB filtering is done on a per-site level. If any sample has VDB below the cutoff it is "\
          "filtered out. If any sample has reads supporting variant allele on both strands this site is retained.")

def read_vdb_metrics(vdb_file):
    with open(vdb_file) as vdb_fh:
        vdb_csv = csv.DictReader(vdb_fh, delimiter = "\t")
        for line in vdb_csv:
            locus = line['chr'] + ":" + line['pos']
            if line['MQSB'] == "NA":
                line['MQSB'] = 0
            if locus not in calls_minvdb:
                calls_minvdb[locus] = float(line['VDB'])
                calls_minmqsb[locus] = float(line['MQSB'])
                calls_robs[locus] = int(line['reads_on_both_strands'])
            if float(line['VDB']) < calls_minvdb[locus]:
                calls_minvdb[locus] = float(line['VDB'])
            if float(line['MQSB']) < calls_minmqsb[locus]:
                calls_minmqsb[locus] = float(line['MQSB'])
            if int(line['reads_on_both_strands']) > 0:
                calls_robs[locus] = int(line['reads_on_both_strands'])

def print_calls_satifying_cutoff(bcall_file, cutoff):
    with open(bcall_file) as bcfh:
        for line in bcfh:
            line = line.rstrip("\n")
            fields = line.split("\t")
            locus = fields[0] + ":" + fields[2]
            if locus not in calls_minvdb:
                print("Unable to find VDB for\t" + fields[0] + "\t" + fields[2] + "\t" + fields[-1], file = sys.stderr)
            elif calls_minvdb[locus] > cutoff and calls_minmqsb[locus] > cutoff and calls_robs[locus] != 0: #Disable MQSB for now.
                fields.insert(-1, str(calls_minvdb[locus]))
                fields.insert(-1, str(calls_minmqsb[locus]))
                print("\t".join(fields))
            else:
                print("Failed VDB cutoff.", "\t".join(fields), calls_robs[locus], file = sys.stderr)

def filter_using_vdb_and_strands(argv):
    if len(argv) < 4:
        return usage()
    vdb_file = argv[1]
    bcall_file = argv[2]
    cutoff = float(argv[3])
    print("The VDB file is ", vdb_file, file = sys.stderr)
    read_vdb_metrics(vdb_file)
    print_calls_satifying_cutoff(bcall_file, cutoff)
