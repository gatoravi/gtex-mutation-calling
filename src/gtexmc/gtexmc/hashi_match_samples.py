#! /usr/bin/env python

import sys
import csv

"""
This is a three way match. The hashi_male_breast_thyroid_27May14_samples.txt file
has columns that look like
     1 CaseID  Gender:Age:BMI
     2 GTEX-000026 Female:61:29.29
The phenotype_data/GTEx_Data_2014-06-13_Annotations_SubjectPhenotypesDS_samples.txt file
has columns that look like
     1 SUBJID  :AGE:BMI
     2 GTEX-N7MS       Male:61:26.28
Use the Gender:Age:BMI to get the get the SUBJID from the GTEX_ file.
There might be multiple subject id's with the same Gender:Age:BMI combination.
Use the SUBJID to get the run_id from SRARunTable.txt, this is the column
that we are looking for and is the same ID that's in our mutations file!
"""

all_map = {}
sra_map = {}

def read_sra_map(sra_run_file):
    with open(sra_run_file) as sra_run_fh:
        for row in csv.DictReader(sra_run_fh, delimiter = "\t"):
            run_s = row['Run_s']
            subject_id = row['submitted_subject_id_s']
            #Look at thyroid samples alone
            if row['histological_type_s'] == "Thyroid":
                if subject_id not in sra_map:
                    sra_map[subject_id] = []
                sra_map[subject_id].append(run_s)

def read_all_map(all_gtex_sample_file):
    all_fh = open(all_gtex_sample_file)
    for line in all_fh:
        line = line.rstrip("\n")
        sample, key = line.split("\t")
        if key not in all_map:
            all_map[key] = []
        all_map[key].append(sample)

def read_and_match_hashi(hashi_sample_file):
    hashi_fh = open(hashi_sample_file)
    for line in hashi_fh:
        line = line.rstrip("\n")
        sample, key = line.split("\t")
        if key in all_map:
            if "".join(all_map[key]) in sra_map:
                print ("match", key, sample, "".join(all_map[key]), \
                        " ".join(sra_map["".join(all_map[key])]))
            else:
                print("".join(all_map[key]), " not found in SRA thyroid map.", file = sys.stderr)

def usage():
    print("Usage:" \
          "\n\tgtexmc hashi-match-samples ../dat/SRARunTable.txt " \
          "../dat/phenotype_data/GTEx_Data_2014-06-13_Annotations_SubjectPhenotypesDS_samples.txt ../dat/phenotype_data/hashi_male_breast_thyroid_27May14_samples.txt")
    return

def hashi_match_samples(argv):
    if len(argv) != 4:
        return usage()
    sra_run_file = argv[1]
    all_gtex_sample_file = argv[2]
    hashi_sample_file = argv[3]
    read_sra_map(sra_run_file)
    read_all_map(all_gtex_sample_file)
    read_and_match_hashi(hashi_sample_file)

if __name__ == "__main__":
    hashi_match_samples(sys.argv)
