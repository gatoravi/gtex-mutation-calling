'''
    nmutations_sample.py

    Copyright: Avinash Ramu <aramu@genome.wustl.edu>

    Author: Avinash Ramu <aramu@genome.wustl.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
'''

class Vcf:
    """
    Holds the results of gtex mutation calling.
    """
    def __init__(self, vcf1, tissue1 = None):
        self.vcf = vcf1
        self.tissue = tissue1
        #Key is SRRid, value is n_mutations
        self.sample_totals = {}

    def add_sample_to_dict(self, sample):
        if sample not in self.sample_totals:
            self.sample_totals[sample] = 1
        else:
            self.sample_totals[sample] += 1

    def parse_info(self, info):
        info_fields = info.split(";")
        for field in info_fields:
            if field.startswith("tissue="):
                key, current_tissue = field.split("=")
            if field.startswith("sample="):
                key, sample = field.split("=")
        if self.tissue != None:
            if current_tissue == self.tissue:
                self.add_sample_to_dict(sample)
        else:#No tissue specified so add all tissues
            self.add_sample_to_dict(sample)

    def parse_vcf(self):
        with open(self.vcf) as vcf_fh:
            for line in vcf_fh:
                if line.startswith('#'):
                    continue
                rest, info, format, sample = line.rsplit("\t", 3)
                self.parse_info(info)
