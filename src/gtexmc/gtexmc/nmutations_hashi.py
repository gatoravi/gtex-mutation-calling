'''
    nmutations_hashi.py

    Copyright: Avinash Ramu <aramu@genome.wustl.edu>

    Author: Avinash Ramu <aramu@genome.wustl.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
'''

import sys
import csv
from .vcf import Vcf

"""
Compare the number of mutations
in hashimoto thyroid samples and
compare to the rest of the samples.
"""

hashi_samples = [] #List of hashimoto samples

def usage():
    print("Usage: gtexmc nmutations-hashi-samples " \
        "../../../2016-05-03-mpileup_all_sites_withVDB_withsampleinfo_withallTags/samtools1.1_AF0_1.vcf  " \
        "../../../dat/SRARunTable.txt "
        "hashi_thyroid_matches.csv")
    print("Counts number of variants in hashi samples")
    return 0

def read_hashi_samples(hashi_matches_file):
    with open(hashi_matches_file) as hashi_matches_fh:
        for line in hashi_matches_fh:
            line = line.rstrip("\n")
            sample = line.split()[4]
            hashi_samples.append(sample)

def print_sample_totals_hashi(vcf1):
    print("sample\tsex\ttotal_n_mutations\thashi_status")
    for sample in vcf1.sample_totals:
        #sex = SRR_to_sex[sample]
        if sample in hashi_samples:
            hashi = True
        else:
            hashi = False
        print(sample, "NA", vcf1.sample_totals[sample], hashi, sep = "\t")

def nmutations_hashi(argv):
    if len(argv) != 4:
        return usage()
    vcf = argv[1]
    sra_run_file = argv[2]
    hashi_matches_file = argv[3]
    vcf1 = Vcf(vcf, "Thyroid")
    vcf1.parse_vcf()
    read_hashi_samples(hashi_matches_file)
    print_sample_totals_hashi(vcf1)
