'''
    filter_using_vdb.py
    Filter the variants using the variant distance bias metric.

    Copyright: Avinash Ramu <aramu@genome.wustl.edu>

    Author: Avinash Ramu <aramu@genome.wustl.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
'''
import sys
import csv

flagged_samples = {}

def usage():
    print("gtexmc filter_flagged_samples " \
          "flagged_samples.tsv " \
          "all-calls.tsv")
    print("Filter out the samples flagged by the GTEx consortium.")

def read_flagged_samples(flagged):
    with open(flagged) as flagged_fh:
        for line in flagged_fh:
            fields = line.split()
            srr_id = fields[1]
            flagged_samples[srr_id] = 1

def filter_flagged_samples(argv):
    if len(argv) < 3:
        return usage()
    flagged = argv[1]
    bcall_file = argv[2]
    read_flagged_samples(flagged)
    print("Number of flagged samples is ", len(flagged_samples), file = sys.stderr)
    with open(bcall_file) as bcall_fh:
        for line in bcall_fh:
            line = line.rstrip("\n")
            fields = line.split("\t")
            srr_id = fields[-1]
            if srr_id not in flagged_samples:
                print(line)
            else:
                print("Removed call from ", srr_id, file = sys.stderr)
