'''
    bcall.py
    Deal with results from bcall

    Copyright: Avinash Ramu <aramu@genome.wustl.edu>

    Author: Avinash Ramu <aramu@genome.wustl.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
'''

import sys
from .sra_runs import SRAMap

class bcallMap:
    def __init__(self, bcall_results1):
        self.bcall_results = bcall_results1
        self.calls_samples = {} #Key is chr:pos, value is list of samples
        self.calls_subjects = {} #Key is chr:pos, value is list of subjects
        self.calls = {} #Key is chr:pos, value is bcall line
        self.sample_totals = {} #Number of calls from each sample
        self.sample_tissue = {} #Map from sample -> tissue
        self.calls_shared_within_subject = {} #Key is call, value is true/false, true if call shared across samples
                                         #of the same individual, hence likely germline in that individual.
        self.individual_cumulative_ref = {} #Key is chr:pos, value is a list of dictionary(key is subjid, val is cumul_ref_count)
        self.individual_cumulative_alt = {} #Key is chr:pos, value is a list of dictionary(key is subjid, val is cumul_alt_count)
        self.individual_nsamples = {} #Key is chr:pos, value is a list of dictionary(key is subjid, val is number_of_samples)

    def add_to_individual_cumulative(self, sra1, sample, call, ref_count, alt_count):
        "Accumulate ref, alt counts at a locus for each individual"
        subject = sra1.SRR_to_subjectid[sample]
        if call not in self.individual_cumulative_ref:
            self.individual_cumulative_ref[call] = {}
            self.individual_cumulative_alt[call] = {}
            self.individual_nsamples[call] = {}
        if subject not in self.individual_cumulative_ref[call]:
            self.individual_cumulative_ref[call][subject] = 0
            self.individual_cumulative_alt[call][subject] = 0
            self.individual_nsamples[call][subject] = 0
        self.individual_cumulative_ref[call][subject] += ref_count
        self.individual_cumulative_alt[call][subject] += alt_count
        self.individual_nsamples[call][subject] += 1
        nsamples = self.individual_nsamples[call][subject]

    def add_call_to_sample_totals(self, sample):
        if sample not in self.sample_totals:
            self.sample_totals[sample] = 1
        else:
            self.sample_totals[sample] += 1

    def read_calls(self, sra_run_file = "NA", tissue_col = -2):
        """
        Specify a SRA run file if you want mapping from sample
        to subject.
        """
        if sra_run_file != "NA":
            print ("sra_run_file is ", sra_run_file, file = sys.stderr)
            sra1 = SRAMap(sra_run_file)
            sra1.read_srr_to_gtex_map()
        print("Make sure ref, alt count is 6th and 7th column", file = sys.stderr)
        with open(self.bcall_results) as br_fh:
            for line in br_fh:
                line = line.rstrip("\n")
                fields = line.split("\t")
                #"The key is chr:pos, make sure these fields are correct"
                call = fields[0] + ":" + fields[1]
                ref_count = int(fields[5])
                alt_count = int(fields[6])
                sample = fields[-1]
                tissue = fields[tissue_col]
                if call not in self.calls:
                    self.calls[call] = []
                    self.calls_samples[call] = []
                #List of lines with the same calls(different samples)
                self.calls[call].append(line)
                #List of samples containing this call
                self.calls_samples[call].append(sample)
                #Maintain a running total of number of calls in this sample
                self.add_call_to_sample_totals(sample)
                #Map from sample to tissue
                self.sample_tissue[sample] = tissue
                if sra_run_file != "NA":
                    self.add_to_individual_cumulative(sra1, sample, call, ref_count, alt_count)
                    subject = sra1.SRR_to_subjectid[sample]
                    #First time this subject,call combination is seen.
                    if call not in self.calls_subjects:
                        #print(call, "not seen before in subject ", subject, file = sys.stderr)
                        self.calls_subjects[call] = [subject]
                        self.calls_shared_within_subject[call] = False
                    #Check if this call is seen in this subject before
                    else:
                        if subject in self.calls_subjects[call]:
                            #print(call, "seen before in subject ", subject, file = sys.stderr)
                            self.calls_shared_within_subject[call] = True
                        else:
                            #print(call, "not seen before in subject ", subject, file = sys.stderr)
                            self.calls_subjects[call].append(subject)
