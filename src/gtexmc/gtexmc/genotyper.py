'''
    genotyper.py
    Figure out the alt-alelle by looking at the readcounts
    at a locus.
    Copyright: Avinash Ramu <aramu@genome.wustl.edu>

    Author: Avinash Ramu <aramu@genome.wustl.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
'''
from .bcall import bcallMap
import sys

def usage():
    print("gtexmc genotyper " \
          "bcall_results.tsv")
    print("Moves total RD column to after ref, alt alleles.")

def genotyper(argv):
    "Filter the mutations shared across tissues of the same individual, likely germline"
    if len(argv) != 2:
        return usage()
    bcall_results_file = argv[1]
    with open(bcall_results_file) as bfh:
        for line in bfh:
            line = line.rstrip("\n")
            fields = line.split("\t")
            ref_allele = fields[4]
            acount, ccount, gcount, tcount = [int(x) for x in fields[7:11]]
            counts = [("A", acount), ("C", ccount), ("G", gcount), ("T", tcount)]
            max1, max2 = 0, 0
            nuc1, nuc2 = "N", "N"
            for nuc, count in counts:
                if count > max1:
                    max2 = max1
                    nuc2 = nuc1
                    max1 = count
                    nuc1 = nuc
                elif count > max2:
                    max2 = count
                    nuc2 = nuc
            if nuc2 == "N":
                nuc2 = nuc1
                max2 = max1
            if ref_allele != nuc1:
                alt_allele = nuc1
            else:
                alt_allele = nuc2
            del fields[4] #Remove ref, allele column and insert it before
            fields.insert(3, ref_allele)
            fields.insert(4, alt_allele)
            #Output format becomes chr\tstart\tend\tref\talt\ttotal_rd\tref_count\alt_count\acount\tccount\tgcount....
            print("\t".join(fields))
