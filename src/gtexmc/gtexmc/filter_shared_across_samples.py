'''
    filter_shared_across_samples.py
    Filter the variants shared across multiple samples

    Copyright: Avinash Ramu <aramu@genome.wustl.edu>

    Author: Avinash Ramu <aramu@genome.wustl.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
'''
from .bcall import bcallMap
import sys

def usage():
    print("gtexmc filter-shared-across-samples " \
          "bcall_results.tsv " \
          "SRARunTable.txt")
    #print("The last argument is the maximum number of tissues that a mutation is allowed to be "
           #"observed in. A mutation that is in <= cutoff number of tissues will be displayed.")

def filter_shared_across_samples(argv):
    "Filter the mutations shared across tissues of the same individual, likely germline"
    if len(argv) != 3:
        return usage()
    bcall_results_file = argv[1]
    sra_run_file = argv[2]
    bm1 = bcallMap(bcall_results_file)
    bm1.read_calls(sra_run_file)
    for locus, all_calls in bm1.calls.items():
        #Only print calls that are not shared amongst samples of the same subject
        if not bm1.calls_shared_within_subject[locus]:
            for single_call in all_calls:
                print(single_call)
        else:
            print("This locus is shared in an individual", all_calls, len(bm1.calls_samples[locus]), file = sys.stderr)
