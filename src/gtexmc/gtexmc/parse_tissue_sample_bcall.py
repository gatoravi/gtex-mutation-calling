#! /usr/bin/env python

import itertools
import sys

#Key is locus, value is list of "sample+tissue"
shared_among_tissues = {}
#Two dimensional list of tissues*tissues
#Values are the total number of mutations observed in the pair
#Hash of hash, keys are tissue1 and tissue2
tissue_matrix = {}
total_sum = 0

def usage():
    print("Get the mutations that are shared between more than one tissue in an individual")
    print("gtexmc parse-tissue-sample-bcall bcall_output.tsv")

def add_to_shared_among_tissues(sample, tissue, locus):
    "Add to shared among tissues"
    global shared_among_tissues
    if sample != "NA" and tissue != "NA":
        if locus not in shared_among_tissues:
            shared_among_tissues[locus] = set()
        shared_among_tissues[locus].add(sample + ":" + tissue)

def parse_fields(vcf_line):
   "Parse a line from bcall"
   fields = vcf_line.split("\t")
   chr = fields[0]
   pos = fields[1]
   locus = chr + ":" + pos
   sample = fields[-1]
   tissue = fields[-2]
   add_to_shared_among_tissues(sample, tissue, locus)

def read_bcall(bcall_f):
    "Read the results from bcall"
    bcall_fh = open(bcall_f)
    for line in bcall_fh:
        if line[0] == "#":
            continue
        line = line.rstrip("\n")
        parse_fields(line)

def print_tissue_matrix():
    "Print out the contents of the tissue matrix to the screen"
    sys.stdout.write("Tissue ")
    for tissue1 in tissue_matrix:
        sys.stdout.write("%s " % tissue1)
    sys.stdout.write("\n")
    for tissue1 in tissue_matrix:
        sys.stdout.write("%s " % tissue1)
        for tissue2 in tissue_matrix:
            if tissue2 in tissue_matrix[tissue1]:
                sys.stdout.write("%d " % tissue_matrix[tissue1][tissue2])
            else:
                sys.stdout.write("0 ")
        sys.stdout.write("\n")

def add_pairs_to_tissue_matrix(pairs_of_tissues):
    #Takes a list of tuples which are pairs of tissues
    #Increment the count of the number of mutations shared
    #amongst these two tissues.
    global tissue_matrix
    global total_sum
    for pair in pairs_of_tissues:
        tissue1, tissue2 = pair
        if tissue1 not in tissue_matrix:
            tissue_matrix[tissue1] = {}
        if tissue2 not in tissue_matrix:
            tissue_matrix[tissue2] = {}
        if tissue1 not in tissue_matrix[tissue2]:
            tissue_matrix[tissue2][tissue1] = 0
        if tissue2 not in tissue_matrix[tissue1]:
            tissue_matrix[tissue1][tissue2] = 0
        tissue_matrix[tissue1][tissue2] += 1
        tissue_matrix[tissue2][tissue1] += 1

def add_to_tissue_matrix(shared_tissues):
    #The value of the hash is of the form sample:tissue
    #Split this to get the tissue name
    #Get all the tissues this call is observed in, then get all permutations of these calls
    tissues = [x.split(":")[1] for x in shared_tissues]
    pairs_of_tissues = set(itertools.product(tissues, repeat = 2))
    add_pairs_to_tissue_matrix(pairs_of_tissues)

def calculate_mutations_shared_tissues_across_individuals():
    "Make pairs from tissues which have the same key"
    sum_calls = 0
    for locus in shared_among_tissues:
        #if(len(shared_among_tissues[locus]) > 1): #count=1 are loci present in just one tissue
        shared_tissues = shared_among_tissues[locus]
        #print "shared among tissues", locus, shared
        add_to_tissue_matrix(shared_tissues)
        sum_calls += len(shared_tissues)
    print("Total number of calls is ", sum_calls, file = sys.stderr)

def parse_tissue_sample_bcall(argv):
    if len(argv) < 2:
        return usage()
    bcall_f = argv[1]
    read_bcall(bcall_f)
    #print_samples_with_mutations_shared_tissues() #in the same individual, these are mostly just duplicates in the input
    calculate_mutations_shared_tissues_across_individuals()
    print_tissue_matrix()
    print("Done!", file = sys.stderr)

if __name__ == "__main__":
    parse_tissue_sample_bcall(sys.argv)
