'''
    add_tissue_to_calls.py
    Add the tissue column to the results from bcall.
    Assumes the SRR id is in the last column

    Copyright: Avinash Ramu <aramu@genome.wustl.edu>

    Author: Avinash Ramu <aramu@genome.wustl.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
'''
from .bcall import bcallMap
import sys
import csv

def usage():
    print("gtexmc add-tissue-to-calls " \
          "bcall_results.tsv " \
          "../dat/SRARunTable.txt")
    print("Adds the tissue name to the calls from bcall")

def read_tissue_subject_map(tissue_subject_map_file):
    "Create a map from SRRid > (tissue, subject)"
    tissue_subject_map = {}
    with open(tissue_subject_map_file) as tmf_fh:
        tmf = csv.DictReader(tmf_fh, delimiter = "\t")
        for row in tmf:
            tissue_subject_map[row['Run_s']] = (row['body_site_s'], \
                                                row['submitted_subject_id_s'])
    return tissue_subject_map

def add_tissue_subject_to_calls(argv):
    "add tissue column to bcall results"
    if len(argv) != 3:
        return usage()
    bcall_results_file = argv[1]
    tissue_subject_map_file = argv[2]
    tissue_subject_map = read_tissue_subject_map(tissue_subject_map_file)
    bm1 = bcallMap(bcall_results_file)
    bm1.read_calls()
    for call in bm1.calls:
        last_col = len(bm1.calls[call]) - 1
        for line in bm1.calls[call]:
            fields = line.split("\t")
            run_id = fields[-1]
            tissue = tissue_subject_map[run_id][0]
            subject = tissue_subject_map[run_id][1]
            fields.insert(-1, subject)
            fields.insert(-1, '"' + tissue + '"')
            print("\t".join(fields))
