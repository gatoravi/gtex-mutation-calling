'''
    nmutations_subject.py
    Calculate number of mutations in each subject

    Copyright: Avinash Ramu <aramu@genome.wustl.edu>

    Author: Avinash Ramu <aramu@genome.wustl.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
'''

from .vcf import Vcf
from .sra_runs import SRAMap
from .bcall import bcallMap

def usage():
    print("gtexmc nmutations-subject-vcf " \
        "../../../2016-05-03-mpileup_all_sites_withVDB_withsampleinfo_withallTags/samtools1.1_AF0_1.vcf  " \
        "../../../dat/SRARunTable.txt " \
        "../dat/phentotype_data/phs000424.v6.pht002742.v6.p1.c1.GTEx_Subject_Phenotypes.GRU.txt")
    print("gtexmc nmutations-subject-bcall " \
        "bcall_results.tsv  " \
        "dat/SRARunTable.txt " \
        "dat/phentotype_data/phs000424.v6.pht002742.v6.p1.c1.GTEx_Subject_Phenotypes.GRU.txt")

#Key is subjectID, value is n_mutations
subject_totals = {}
#Key is subjectID, value is n_tissues
subject_ntissues = {}

def print_subject_totals(sra1):
    "Print the subjectwide totals"
    print("subject\tage\tn_tissues\tn_mutations\tnormalized_n_mutations")
    for subject in subject_totals:
        n_mutations = subject_totals[subject]
        n_tissues = subject_ntissues[subject]
        normalized_nmut = float(n_mutations)/float(n_tissues)
        print(subject, sra1.gtex_to_age[subject],
              n_tissues,
              n_mutations,
              normalized_nmut,
              sep = "\t")

def sum_across_subject(variants1, sra1):
    "Sum the number of mutations across all samples for a subject"
    for sample in variants1.sample_totals:
        subject = sra1.SRR_to_subjectid[sample]
        if subject not in subject_totals:
            subject_totals[subject] = 0
            subject_ntissues[subject] = 0
        subject_totals[subject] += variants1.sample_totals[sample]
        subject_ntissues[subject] += 1

def nmutations_subject_bcall(argv):
    "Get the number of mutations in each subject"
    if len(argv) != 4:
        return usage()
    bcall_results = argv[1] #n_mut in each tissue
    sra_run_file = argv[2] #tissue to subject map
    gtex_pheno = argv[3] #this file has the ages
    bcall1 = bcallMap(bcall_results)
    bcall1.read_calls()
    sra1 = SRAMap(sra_run_file, gtex_pheno)
    sra1.read_srr_to_gtex_map()
    sra1.read_subjectid_to_age_map()
    sum_across_subject(bcall1, sra1)
    print_subject_totals(sra1)

def nmutations_subject_vcf(argv):
    "Get the number of mutations in each subject"
    if len(argv) != 4:
        return usage()
    vcf = argv[1] #n_mut in each tissue
    sra_run_file = argv[2] #tissue to subject map
    gtex_pheno = argv[3] #this file has the ages
    vcf1 = Vcf(vcf)
    vcf1.parse_vcf()
    sra1 = SRAMap(sra_run_file, gtex_pheno)
    sra1.read_srr_to_gtex_map()
    sra1.read_subjectid_to_age_map()
    sum_across_subject(vcf1, sra1)
    print_subject_totals(sra1)
