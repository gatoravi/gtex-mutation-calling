'''
    nmutations_sample.py

    Copyright: Avinash Ramu <aramu@genome.wustl.edu>

    Author: Avinash Ramu <aramu@genome.wustl.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
'''

import sys
import csv
from .vcf import Vcf
from .sra_runs import SRAMap
from .bcall import bcallMap

#Map from tissue to number of samples in this tissue
tissue_nsamples = {}
#Map from tissue to number of calls in this tissue
tissue_ncalls = {}

def usage():
    print("gtexmc nmutations-sample-bcall bcall_results.tsv  " \
        "../dat/tissue_summary.txt")
    print("Counts number of variants in each tissue-type. Adjust for number of samples for each tissue-type.")
    return 0

def read_tissues_in_bcall(bcall_f):
    "Create the map of tissue to number of calls in that tissue"
    global tissue_ncalls
    with open(bcall_f) as bcall_fh:
        for line in bcall_fh:
            line = line.rstrip("\n")
            fields = line.split("\t")
            tissue = fields[-2]
            tissue = tissue.replace('"', '')
            tissue = tissue.replace(' ', '')
            if tissue not in tissue_ncalls:
                tissue_ncalls[tissue] = 0
            tissue_ncalls[tissue] += 1

def read_tissues_nsamples(tissue_summary_f):
    global tissue_nsamples
    with open(tissue_summary_f) as tsfh:
        next(tsfh) #skip header
        nsamples = 0
        for line in tsfh:
            line = line.rstrip("\n")
            fields = line.split("\t")
            tissue = fields[0]
            tissue = tissue.replace(' ', '')
            ncount = fields[1]
            ncount = int(ncount)
            tissue_nsamples[tissue] = ncount
            nsamples += ncount
        print("Total number of samples is ", nsamples, file = sys.stderr)

def print_ncalls_nsamples():
    global tissue_ncalls
    global tissue_nsamples
    print("Tissue", "n_samples", "n_calls", "n_normalized_calls")
    for tissue in tissue_ncalls:
        print(tissue, tissue_nsamples[tissue], tissue_ncalls[tissue], tissue_ncalls[tissue]/tissue_nsamples[tissue])

def nmutations_tissue_bcall(argv):
    if len(argv) != 3:
        return usage()
    bcall_results_file = argv[1]
    tissue_summary = argv[2]
    read_tissues_in_bcall(bcall_results_file)
    read_tissues_nsamples(tissue_summary)
    print_ncalls_nsamples()
