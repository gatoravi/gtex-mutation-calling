'''
    filter_max_variant_alleles.py
    Filter out the sites which have read support for more than two alleles
    Copyright: Avinash Ramu <aramu@genome.wustl.edu>

    Author: Avinash Ramu <aramu@genome.wustl.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
'''
from .bcall import bcallMap
import sys
import csv

rd_cutoff = 2
#Dictionary of sites which fail filter, key is chr:pos, value is True
omit_sites = {}

def usage():
    print("gtexmc filter-max-variant-alleles " \
          "bcall_results.tsv ")
    print("Filter sites which have read support(>2) for more than two alleles in "\
          "any sample. Make sure fields[8-11] are a,c,g,t counts.")

def filter_max_variant_alleles(argv):
    if len(argv) != 2:
        return usage()
    bcall_results_file = argv[1]
    all_lines = []
    with open(bcall_results_file) as bcfh:
        for line in bcfh:
            all_lines.append(line)
            line = line.rstrip('\n')
            fields = line.split()
            locus = fields[0] + ":" + fields[1]
            acount = int(fields[8])
            ccount = int(fields[9])
            gcount = int(fields[10])
            tcount = int(fields[11])
            n_ge2 = 0
            for count in [acount, ccount, gcount, tcount]:
                if count > rd_cutoff:
                    n_ge2 += 1
            if n_ge2 > 2: #More than two alleles pass the read-depth cutoff
                print(line, "filtered-out", file = sys.stderr)
                omit_sites[locus] = True
    for line in all_lines:
        line = line.rstrip('\n')
        fields = line.split()
        locus = fields[0] + ":" + fields[1]
        if locus not in omit_sites:
            print(line)
