'''
    filter_individual_cumulative_vaf.py
    Filter the variants using the cumulative VAF across
    all tissues for an individual at a locus.

    Copyright: Avinash Ramu <aramu@genome.wustl.edu>

    Author: Avinash Ramu <aramu@genome.wustl.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
'''
from .bcall import bcallMap
import sys

def usage():
    print("gtexmc filter-individual-cumulative-vaf " \
          "bcall_results.tsv " \
          "SRARunTable.txt " \
          "VAF_cutoff(0-1)")
    print("Make sure that the 7th and 8th columns are ref and alt counts.")

def filter_individual_cumulative_vaf(argv):
    "Filter the mutations shared across tissues of the same individual, likely germline"
    if len(argv) != 4:
        return usage()
    bcall_results_file = argv[1]
    sra_run_file = argv[2]
    vaf_cutoff = float(argv[3])
    print("Before reading calls!", file = sys.stderr)
    bm1 = bcallMap(bcall_results_file)
    bm1.read_calls(sra_run_file)
    print("VAF cutoff is: ", vaf_cutoff, file = sys.stderr)
    for locus, all_calls in bm1.calls.items():
        #Only print calls that are below the VAF threshold in all indiv"
        greatest_vaf = 0
        vaf_greater_than_cutoff = False
        for subject in bm1.individual_cumulative_ref[locus]:
            ref_count = bm1.individual_cumulative_ref[locus][subject]
            alt_count = bm1.individual_cumulative_alt[locus][subject]
            n_samples = bm1.individual_nsamples[locus][subject] #Number of samples in this individual containing this call
            vaf = alt_count/(ref_count + alt_count)
            if n_samples >1 and vaf > vaf_cutoff: #Check that this is in multiple tissues of same individual
                vaf_greater_than_cutoff = True
                greatest_vaf = vaf
                break
        if not vaf_greater_than_cutoff: #Doesn't look germline in any subject
            for call in all_calls: #Print this locus' call in all subjects
                print(call)
        else: #print the filtered out calls to STDERR
            for call in all_calls:
                print(call, "FILTERED_OUT", file = sys.stderr)
