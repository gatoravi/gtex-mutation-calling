'''
    nmutations_sample.py

    Copyright: Avinash Ramu <aramu@genome.wustl.edu>

    Author: Avinash Ramu <aramu@genome.wustl.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
'''

import sys
import csv
from .vcf import Vcf
from .sra_runs import SRAMap
from .bcall import bcallMap

def usage():
    print("Usage: gtexmc nmutations-sample-vcf ../../../2016-05-03-mpileup_all_sites_withVDB_withsampleinfo_withallTags/samtools1.1_AF0_1.vcf  " \
        "../../../dat/SRARunTable.txt   ../../../dat/Tuuli_SI/GTEx_Analysis_v6_ASE.exome.chrX_stats.zero.txt" \
        " ../dat/phenotype_data/phs000424.v6.pht002742.v6.p1.c1.GTEx_Subject_Phenotypes.GRU.txt")
    print("Counts number of variants in each sample. Also correlates with S_I statistic and AGE.")
    print("gtexmc nmutations-sample-bcall bcall_results.tsv  " \
        "../../../dat/SRARunTable.txt   ../../../dat/Tuuli_SI/GTEx_Analysis_v6_ASE.exome.chrX_stats.zero.txt" \
        " ../dat/phenotype_data/phs000424.v6.pht002742.v6.p1.c1.GTEx_Subject_Phenotypes.GRU.txt" \
        " ../dat/flagged_samples.tsv")
    print("Counts number of variants in each sample. Also correlates with S_I statistic and AGE.")
    return 0

SI_map = {} #Store the SI statistic, skewed_inactivation 0 to 1, higher indicates more skewing
flagged_samples = {}#Store the flagged samples

def read_si_table(si_table_file):
    with open(si_table_file) as si_table_fh:
        for row in csv.DictReader(si_table_fh, delimiter = "\t"):
            sample = row['sample']
            perc = row['monoall_X_perc']
            SI_map[sample] = float(perc)

def print_sample_totals_SI(variants1, sra1):
    print("sample\tsubject\ttissue\tsex\tage\ttotal_n_mutations\tSI_metric")
    for sample in variants1.sample_totals:
        tissue = variants1.sample_tissue[sample]
        GTEX_id = sra1.SRR_to_samplename_map[sample]
        sex = sra1.SRR_to_sex[sample]
        subjectid = sra1.SRR_to_subjectid[sample]
        age = sra1.gtex_to_age[subjectid]
        if GTEX_id in SI_map:
            print(sample, subjectid, tissue, sex, age, variants1.sample_totals[sample], SI_map[GTEX_id], sep = "\t")
        else:
            print(sample, subjectid, tissue, sex, age, variants1.sample_totals[sample], "NA", sep = "\t")
    for GTEX_id in sra1.gtex_to_SRR_map:
        if GTEX_id in SI_map:
            SI = SI_map[GTEX_id]
        else:
            SI = "NA"
        sample = sra1.gtex_to_SRR_map[GTEX_id]
        sex = sra1.SRR_to_sex[sample]
        subjectid = sra1.SRR_to_subjectid[sample]
        age = sra1.gtex_to_age[subjectid]
        if sample not in variants1.sample_totals and sample not in flagged_samples:
            tissue = sra1.SRR_to_tissue[sample]
            print(sample, subjectid, tissue, sex, age, 0, SI, sep = "\t")

def read_flagged(flagged):
    with open(flagged) as ffh:
        for line in ffh:
            fields = line.split("\t")
            sample = fields[1]
            flagged_samples[sample] = 1

def nmutations_sample_bcall(argv):
    if len(argv) != 6:
        return usage()
    bcall_results_file = argv[1]
    sra_run_file = argv[2]
    si_table = argv[3]
    gtex_pheno = argv[4]
    flagged = argv[5]
    read_flagged(flagged)
    sra1 = SRAMap(sra_run_file, gtex_pheno)
    sra1.read_srr_to_gtex_map()
    sra1.read_subjectid_to_age_map()
    read_si_table(si_table)
    bcall1 = bcallMap(bcall_results_file)
    bcall1.read_calls(tissue_col = -4)
    print_sample_totals_SI(bcall1, sra1)

def nmutations_sample_vcf(argv):
    if len(argv) != 5:
        return usage()
    vcf = argv[1]
    sra_run_file = argv[2]
    si_table = argv[3]
    gtex_pheno = argv[4]
    sra1 = SRAMap(sra_run_file, gtex_pheno)
    sra1.read_srr_to_gtex_map()
    sra1.read_subjectid_to_age_map()
    read_si_table(si_table)
    vcf1 = Vcf(vcf)
    vcf1.parse_vcf()
    print_sample_totals_SI(vcf1, sra1)
