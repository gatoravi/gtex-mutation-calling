import os

from setuptools import setup

def get_version():
    import ast

    with open(os.path.join("gtexmc", "__init__.py"), "r") as init_file:
        module = ast.parse(init_file.read())

    version = (ast.literal_eval(node.value) for node in ast.walk(module)
            if isinstance(node, ast.Assign)
            and node.targets[0].id == "__version__")
    try:
        return next(version)
    except StopIteration:
        raise ValueError("version could not be located")

setup(version=get_version(),
      name='gtexmc',
      description="GTEx mutation calling project",
      packages=['gtexmc'],
      long_description=open('README.md').read(),
      author="Avinash Ramu",
      author_email="avinash3003@yahoo.co.in",
      zip_safe=False,
      test_suite='nose.collector',
      include_package_data=True,
      install_requires=['scipy', 'numpy',
      ],
      tests_require=['nose'],
      entry_points = {
          'console_scripts': ['gtexmc=gtexmc.main:main'],
      },
      classifiers=[
            'Development Status :: 4 - Beta',
            'Intended Audience :: Science/Research',
            'License :: OSI Approved :: MIT License',
            'Topic :: Scientific/Engineering :: Bio-Informatics'])
