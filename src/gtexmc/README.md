## To get all the samples for a given subject
- gtexmc subject-to-samples ../../../dat/SRARunTable.txt ../../../dat/samplelist.txt

## To identify bcall results in only one sample
- gtexmc filter-shared-across-samples ../../../2016-0616-applymodel/GTEX-12WSJ_calls.tsv 1
