#! /usr/bin/env python

tissue_summary = "../dat/sample_tissue_list.txt"
#Map from tissue to number of samples
tissue_ncount = {}
with open(tissue_summary) as tissue_summary_fh:
    for line in tissue_summary_fh:
        tissue = line.split()[1]
        if tissue not in tissue_ncount:
            tissue_ncount[tissue] = 0
        tissue_ncount[tissue] += 1

print("Tissue\tn_samples")
for tissue, ncount in tissue_ncount.items():
    print(tissue, "\t", ncount)
