import csv


flagged_samples = {}
flagged_criteria = {} #Key is sample-id, value is criteria

"""Retain these
47 Clinical Exclusion
94 K-562 Control
2 Kleinfelter(XXY)
27 XXY Sex Check
5 dup
"""

def read_flagged_samples():
    "Figure out which samples to exclude"
    flagged_but_include = ["Clinical Exclusion", "K-562 Control", "Kleinfelter(XXY)", "XXY Sex Check", "dup"]
    with open("../dat/phenotype_data/phs000424.v6.pht002743.v6.p1.c1.GTEx_Sample_Attributes.GRU.txt") as flagged_fh:
        flagged_csvd = csv.DictReader(flagged_fh, delimiter = "\t")
        for line in flagged_csvd:
            if line['SMTORMVE'] == "FLAGGED":
                if line['SMFLGRMRK'] not in flagged_but_include:
                    flagged_samples[line['SAMPID']] = 1
                    flagged_criteria[line['SAMPID']] = line['SMFLGRMRK']

def get_flagged_sra_id():
    "Get the SRR ID using the GTEX-*"
    with open("../dat/SRARunTable.txt") as sra_fh:
        sra_csvd = csv.DictReader(sra_fh, delimiter = "\t")
        for line in sra_csvd:
            full_sampid =  line['Sample_Name_s']
            sra_id  = line['Run_s']
            if full_sampid in flagged_samples:
                print("\t".join((full_sampid, sra_id, flagged_criteria[full_sampid])))

def main():
    read_flagged_samples()
    get_flagged_sra_id()

main()
