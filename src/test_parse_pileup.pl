#!/usr/bin/perl
use warnings;
use strict;
require "parse_pileup_query.pl"; #NEED PARSE PILEUP LIBRARY
my $pileup_file = "/tmp/mpileup_test.out";
my $minbasequal = 0; # MINIMUM BASE QUALITY SCORE
my $offset = 33; #BASE QUALITY SCORE OFFSET - 33 FOR SANGER SCALE, 64 FOR ILLUMINA SCALE

open (my $PILEUP, "<", $pileup_file);
while(<$PILEUP>) {
    chomp;
    my ($refbase,$refnuccounts, $acount, $tcount, $ccount, $gcount) = &parse_pileup($_, $minbasequal, $offset);# parse each line of pileup
    print "\n", join "\t", $refbase, $refnuccounts, $acount, $tcount, $ccount, $gcount;
    print "\n", $refbase;
}
