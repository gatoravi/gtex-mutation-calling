#Parse the potentiome to get a count of possible
#synonymous and non-synonymous mutations for each
#gene

import csv
import sys

#Key is gene name, value is syn count
genes_syn_count = {}
#Key is gene name, value is nonsyn count
genes_nonsyn_count = {}
#List of all the genes
genes = set()

def usage():
    print("parse_potentiome.py potentiome_file.tsv")
    print("An example is at /scratch/dclab/potentiome/out/")

def accumulate_counts(potentiome):
    pt_fh = open(potentiome)
    pt_dict = csv.DictReader(pt_fh, delimiter = "\t")
    for line in pt_dict:
        if line['ExonicFunc.wgEncodeGencodeBasicV19'] != "NA":
            gene = line['AAChange.wgEncodeGencodeBasicV19'].split(":")[0]
            function = line['ExonicFunc.wgEncodeGencodeBasicV19']
            #Check for non-synonymous
            if "stoploss" in function or \
               "stopgain" in function or \
               "nonsynonymous" in function :
                if gene not in genes_nonsyn_count:
                    genes_nonsyn_count[gene] = 0
                genes_nonsyn_count[gene] += 1
                genes.add(gene)
            #Check for synonymous
            elif "synonymous" in function:
                if gene not in genes_syn_count:
                    genes_syn_count[gene] = 0
                genes_syn_count[gene] += 1
                genes.add(gene)

def print_counts():
    "Print the syn, non_syn counts"
    print("gene", "n_synonymous", "n_nonsynonymous")
    for gene in genes:
        syn_count = 0
        nonsyn_count = 0
        if gene in genes_syn_count:
            syn_count = genes_syn_count[gene]
        if gene in genes_nonsyn_count:
            nonsyn_count = genes_nonsyn_count[gene]
        print(gene, syn_count, nonsyn_count)

def main():
    if len(sys.argv) < 2:
        return usage()
    accumulate_counts(sys.argv[1])
    print_counts()

main()
