import glob
import sys
import csv

#Dictionary with accumualted counts
#Key is gene-name, value is count
genes_syn_counts = {}
genes_nonsyn_counts = {}

def usage():
    print("\n".join(("This module accumulates counts of syn, nonsyn codons across genes",
          "Some genes are of the form of gene1, gene2",
          "The format of the file is 'gene n_synonymous n_nonsynonymous'",
          "The program takes a prefix and finds all files with that prefix.",
          "./" + sys.argv[0] + " count_files_prefix")))

def accumulate_counts(files):
    "Accumulate gene-level counts"
    for file1 in files:
        with open(file1) as fh:
            fcd = csv.DictReader(fh, delimiter = " ")
            for line in fcd:
                genecol = line['gene']
                genes = genecol.split(",")
                for gene in genes:
                    if gene not in genes_syn_counts:
                        genes_syn_counts[gene] = 0
                        genes_nonsyn_counts[gene] = 0
                    genes_syn_counts[gene] += int(line['n_synonymous'])
                    genes_nonsyn_counts[gene] += int(line['n_nonsynonymous'])

def print_counts():
    "Print the accumulated readcounts"
    print("\t".join(("gene", "n_synonymous", "n_nonsynonymous")))
    for gene in genes_syn_counts:
        print("\t".join((gene, str(genes_syn_counts[gene]), str(genes_nonsyn_counts[gene]))))

def main():
    if len(sys.argv) < 2:
        return usage()
    prefix = sys.argv[1] + "*"
    files = glob.glob(prefix)
    accumulate_counts(files)
    print_counts()

main()
