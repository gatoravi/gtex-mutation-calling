#Parse potentiome totals and bcall totals and compute
# ka/ks ratio for each gen
#It is essentially a ratio of two ratios, the numerators
#come from the bcall file, the denominators come from
#the potentiome file.

import csv
import sys

#Genes with bcall results
genes_bcall = []
#Key is gene name, value is syn count
genes_syncount_total = {}
#Key is gene name, value is nonsyn count
genes_nonsyncount_total = {}
#List of all the genes
genes = set()
genes_no_denom = 0

def usage():
    print("compute_ka_ks.py potentiome_syn_nonsyn_counts bcall_syn_nonsyn_counts")

def read_denoms(potentiome):
    with open(potentiome) as pfh:
        pfh_csv = csv.DictReader(pfh, delimiter = "\t")
        for line in pfh_csv:
            gene = line['gene']
            syncount = int(line['n_synonymous'])
            nonsyncount = int(line['n_nonsynonymous'])
            genes_syncount_total[gene] = syncount
            genes_nonsyncount_total[gene] = nonsyncount

def compute_ka_ks(bcall):
    with open(bcall) as bfh:
        global genes_no_denom
        bfh_csv = csv.DictReader(bfh, delimiter = "\t")
        print("gene", "nonsyncount", "syncount", "gene_nonsyncount_total", \
              "gene_syncount_total", "ka_ks", sep = "\t")
        for line in bfh_csv:
            gene = line['gene']
            genes_bcall.append(gene)
            syncount = int(line['n_synonymous'])
            nonsyncount = int(line['n_nonsynonymous'])
            if gene not in genes_syncount_total or gene not in genes_nonsyncount_total:
                #print("No denominator for gene " + gene, file = sys.stderr)
                genes_no_denom += 1
            elif genes_nonsyncount_total[gene] == 0 or genes_syncount_total[gene] == 0:
                print(gene, "has total syn/nonsyncount zero.", file = sys.stderr)
            elif syncount == 0 or nonsyncount == 0:
                print(gene, "has syncount zero.", syncount, nonsyncount, file = sys.stderr)
            else:
                numerator = nonsyncount/genes_nonsyncount_total[gene]
                denominator = syncount/genes_syncount_total[gene]
                ka_ks = numerator/denominator
                print(gene, nonsyncount, syncount, genes_nonsyncount_total[gene], \
                      genes_syncount_total[gene], ka_ks, sep = "\t")


def main():
    if len(sys.argv) < 3:
        return usage()
    potentiome = sys.argv[1]
    bcall = sys.argv[2]
    print("Potentiome counts are in ", potentiome, file = sys.stderr)
    print("Bcall counts are in ", bcall, file = sys.stderr)
    read_denoms(potentiome)
    compute_ka_ks(bcall)
    print("Total number of genes with bcall results is ", len(genes_bcall), file = sys.stderr)
    print("Number of genes with no potentiome denominators is ", genes_no_denom, file = sys.stderr)

main()
