#Parse the annovar annotations of the calls from bcall
#Accumulate the number of synonymous and non-synonymous
#mutations in each gene

import csv
import sys

#Key is gene name, value is syn count
genes_syn_count = {}
#Key is gene name, value is nonsyn count
genes_nonsyn_count = {}
#List of all the genes
genes = set()

def usage():
    print("parse_annovar_output.py annovar_results.tsv")
    print("An example is at /scratch/dclab/ramua/test/test.avoutput.exonic_variant_function")

def accumulate_counts(annovar_op):
    annovar_fh = open(annovar_op)
    for line in annovar_fh:
        fields = line.split("\t")
        gene = fields[2].split(":")[0]
        #Check for non-synonymous
        if "stoploss" in fields[1] or \
           "stopgain" in fields[1] or \
           "nonsynonymous" in fields[1] :
            if gene not in genes_nonsyn_count:
               genes_nonsyn_count[gene] = 0
            genes_nonsyn_count[gene] += 1
            genes.add(gene)
        #Check for synonymous
        elif "synonymous" in fields[1]:
            if gene not in genes_syn_count:
                genes_syn_count[gene] = 0
            genes_syn_count[gene] += 1
            genes.add(gene)

def print_counts():
    "Print the syn, non_syn counts"
    print("gene", "n_synonymous", "n_nonsynonymous", sep = "\t")
    for gene in genes:
        syn_count = 0
        nonsyn_count = 0
        if gene in genes_syn_count:
            syn_count = genes_syn_count[gene]
        if gene in genes_nonsyn_count:
            nonsyn_count = genes_nonsyn_count[gene]
        print(gene, syn_count, nonsyn_count, sep = "\t")

def main():
    if len(sys.argv) < 2:
        return usage()
    accumulate_counts(sys.argv[1])
    print_counts()

main()
