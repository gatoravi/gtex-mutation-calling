#! /usr/bin/env bash

subset=$1
rm -f log/${subset}.e && rm -f ${subset}_bcall.tsv && bsub -g /aramu/bcall -N -e log/${subset}.e  -o ${subset}_bcall.tsv  -R "select[mem>8000] rusage[mem=8000]" -M 8000000 "LD_LIBRARY_PATH=/gscmnt/gc2607/mardiswilsonlab/aramu/src/gcc-5.2.0/build/lib64/:$LD_LIBRARY_PATH ../bin/bcall-0.3.2 call-using-merged ../2016-06-05-generate-priors-fixed/sample_subsets/${subset} /gscmnt/gc2607/mardiswilsonlab/aramu/test/gtex/2016-06-05-generate-priors-fixed/prior_dumps/merged.dump"
