import sys
import myvariant

def usage():
    print("python get-variant-info.py chr1:2000-2000")

def main():
    if len(sys.argv) < 2:
        usage()
    region = sys.argv[1]
    print(region)
    mv = myvariant.MyVariantInfo()
    v = mv.query(region, fields = 'dbsnp')
    print(v)

if __name__ == "__main__":
    main()
