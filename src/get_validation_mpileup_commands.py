#samtools mpileup  -f /scratch/dclab/genomes/GRCh37/genome.fa GTEX-N7MS-0826.bam -r 3:195442140-195442140  | mpileup2readcounts

import csv
import sys
import os.path
import subprocess

def usage():
    print "python ", sys.argv[0], " validation_candidates.gtex_id.tsv"

def mpileup(locus, sample, description):
    sample_fields = sample.split("-")
    sample = "-".join(sample_fields[0:-2])
    bam = "/scratch/dclab/gtex/mut/vald/bams/" + sample + ".bam"
    if os.path.exists(bam):
        mpileup = ("".join(("samtools mpileup  -f /scratch/dclab/genomes/GRCh37/genome.fa ",
                            bam, " -r ", locus, " | mpileup2readcounts")))
        output = subprocess.check_output(mpileup, shell=True)
        counts = output.split("\n")[1] #Skip the header from mpileup2readcounts
        print("\t".join((counts, sample, description)))

def generate_mpileups(valdn):
    with open(valdn) as vfh:
        vfhcsv = csv.DictReader(vfh, delimiter = "\t")
        for line in vfhcsv:
            locus = line['chrom'] + ":" + line['pos'] + "-" + line['pos']
            for sample in line['preferred.mut.tissue'].split(","):
                mpileup(locus, sample, "preferred")
            for sample in line['preferred.control.tissue'].split(","):
                mpileup(locus, sample, "control")

def main():
    if len(sys.argv) != 2:
        usage()
    valdn = sys.argv[1]
    generate_mpileups(valdn)

main()
