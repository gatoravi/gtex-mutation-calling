#!/bin/perl
############################################################
#Author: Gokul
#perl script that queries editing level of known sites in a BAM file
#This was modified by Don to spit out A, C, G, T counts
use warnings;
use strict;
require "parse_pileup_query.pl"; #NEED PARSE PILEUP LIBRARY

if (@ARGV != 3) {
    die "need to provide 3 input:Edit Site list, INDEXED BAM alignment file and output file name\n";
}
my ($inputfile, $bamfile, $outputfile) = ($ARGV[0], $ARGV[1], $ARGV[2]);

#GLOBAL VARIABLES - PLEASE MODIFY THESE

my $minbasequal = 20; # MINIMUM BASE QUALITY SCORE
my $minmapqual = 30; # MINIMUM READ MAPPING QUALITY SCORE
my $sampath = "samtools"; #PATH TO THE SAMTOOLS EXECUTABLE
my $genomepath = "/gscmnt/gc2719/halllab/users/cchiang/projects/gtex/rna_editing_2015-07-10/Homo_sapiens_assembly19.fasta"; #PATH TO REFERENCE GENOME
my $offset = 33; #BASE QUALITY SCORE OFFSET - 33 FOR SANGER SCALE, 64 FOR ILLUMINA SCALE

##END GLOBAL VARIABLES

my $bedtemp = join '', $outputfile, '.bed';
system("awk \'\$1\!\=\"chromosome\"\{print \$1\"\t\"\$2-1\"\t\"\$2\}\' $inputfile \> $bedtemp");
my $piletemp = join '', $outputfile, '.pileup';
system("$sampath mpileup -A -B -d 1000000 -q $minmapqual -Q $minbasequal -f $genomepath -l $bedtemp $bamfile \> $piletemp");

my %sitehash;

open (my $PILEUP, "<", $piletemp);
while(<$PILEUP>) {
    chomp;
    my ($chr, $position, $refnuc, $coverage, $pile, $qual) = split;
    my $location = join '_', $chr, $position;
    my ($refbase,$refnuccounts, $acount, $tcount, $ccount, $gcount) = &parse_pileup($_, $minbasequal, $offset);# parse each line of pileup
    my $out = join "\t", $chr,$position,$refbase,$refnuccounts, $acount, $ccount, $gcount, $tcount;
    $sitehash{$location}=$out;
}
close $PILEUP;
system("rm $bedtemp");
system("rm $piletemp");

open (my $INPUT , "<", $inputfile) or die "error opening inputfile: $!\n";
open (my $OUTPUT, ">", $outputfile);

while (<$INPUT>) { #READ IN LIST OF KNOWN EDITED SITES AND QUERY EDITING STATUS
        chomp;
        my @fields = split;
        next if ($fields[0] eq 'chromosome');
        my ($chr, $position) = ($fields[0], $fields[1]);
        my $location = join '_', $chr, $position;
        my $gene=$fields[2];
        if ($sitehash{$location}) { #PRINT OUT RESULT
            my $out=$sitehash{$location};
            print $OUTPUT "$gene\t$out\n"; 
        } else {
            print $OUTPUT "$gene\t$chr\t$position\tblah\t0\t0\t0\t0\n";
        }
    }
close $INPUT;
close $OUTPUT;
