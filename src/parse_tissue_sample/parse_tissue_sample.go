package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strings"
)

func coordinates_to_samples() (rs map[string]map[string][]string) {
	return make(map[string]map[string][]string)
}

func samples_to_tissues() (r map[string][]string) {
	return make(map[string][]string)
}

func main() {
	variants := flag.String("variants", "", "VCF file with variants")
	flag.Parse()
	fmt.Printf("variants is %s\n", *variants)
	variants_fh, err := os.Open(*variants)
	if err != nil {
		fmt.Println("Error opening file")
		return
	}
	scanner := bufio.NewScanner(variants_fh)
	for scanner.Scan() {
		//fmt.Printf("%s\n", scanner.Text())
		if !strings.HasPrefix(scanner.Text(), "#") {
			fields := strings.Split(scanner.Text(), "\t")
			chr := fields[0]
			pos := fields[1]
			locus := chr + ":" + pos
			fmt.Printf("Locus is %s\n", locus)
			info := fields[7]
			info_fields := strings.Split(info, ";")
			for _, info_field := range info_fields {
				if strings.HasPrefix(info_field, "sample=") {
					sample := strings.Trim(info_field, "sample=")
					fmt.Printf("sample is %s\n", sample)
				}
				if strings.HasPrefix(info_field, "tissue=") {
					tissue := strings.Trim(info_field, "tissue=")
					fmt.Printf("tissue is %s\n", tissue)
				}
			}
		}
	}
}
