#! /usr/bin/env python

import itertools
import sys

shared_among_individuals = {}
shared_among_tissues = {}
#Two dimensional list of tissues*tissues
#Values are the total number of mutations observed in the pair
#Hash of hash, keys are tissue1 and tissue2
tissue_shared_matrix = {}

def usage():
    print "Get the mutations that are shared between more than one tissue in an individual"
    print "python parse_tissue_sample.py ../../2016-04-22-mpileup_all_sites_withVDB_withsampleinfo/samtools1.1_VDBgreater0_1.vcf"

def parse_info(infos, locus):
    sample = tissue = "NA"
    for info in infos.split(";"):
        if(info.startswith("sample=")):
            sample = info.replace("sample=", "")
        if(info.startswith("tissue=")):
            tissue = info.replace("tissue=", "")
    if sample != "NA" and tissue != "NA":
        if sample not in shared_among_individuals:
            shared_among_individuals[sample] = {}
        if locus not in shared_among_individuals[sample]:
            shared_among_individuals[sample][locus] = []
        shared_among_individuals[sample][locus].append(tissue)
        if locus not in shared_among_tissues:
            shared_among_tissues[locus] = []
        shared_among_tissues[locus].append(sample + ":" + tissue)


def read_vcf_line(vcf_line):
   fields = vcf_line.split("\t")
   chr = fields[0]
   pos = fields[1]
   locus = chr + ":" + pos
   info = fields[7]
   parse_info(info, locus)

def read_vcf(vcf_f):
    vcf_fh = open(vcf_f)
    for line in vcf_fh:
        if line[0] == "#":
            continue
        read_vcf_line(line)

def print_samples_with_mutations_shared_tissues():
    for sample in shared_among_individuals:
        for locus in shared_among_individuals[sample]:
            if(len(shared_among_individuals[sample][locus]) > 1):
                print "shared among indiv", sample, locus, shared_among_individuals[sample][locus]

def print_tissue_matrix():
    sys.stdout.write("Tissue ")
    for tissue1 in tissue_shared_matrix:
        sys.stdout.write("%s " % tissue1)
    sys.stdout.write("\n")
    for tissue1 in tissue_shared_matrix:
        sys.stdout.write("%s " % tissue1)
        for tissue2 in tissue_shared_matrix:
            if tissue2 in tissue_shared_matrix[tissue1]:
                sys.stdout.write("%d " % tissue_shared_matrix[tissue1][tissue2])
            else:
                sys.stdout.write("0 ")
        sys.stdout.write("\n")

def add_pairs_to_tissue_matrix(pairs_of_tissues):
    #Takes a list of tuples which are pairs of tissues
    for pair in pairs_of_tissues:
        tissue1, tissue2 = pair
        if tissue1 not in tissue_shared_matrix:
            tissue_shared_matrix[tissue1] = {}
        if tissue2 not in tissue_shared_matrix:
            tissue_shared_matrix[tissue2] = {}
        if tissue1 not in tissue_shared_matrix[tissue2]:
            tissue_shared_matrix[tissue2][tissue1] = 0
        if tissue2 not in tissue_shared_matrix[tissue1]:
            tissue_shared_matrix[tissue1][tissue2] = 0
        tissue_shared_matrix[tissue1][tissue2] += 1
        tissue_shared_matrix[tissue2][tissue1] += 1

def add_to_tissue_matrix(shared):
    #The value of the hash is of the form sample:tissue
    #Split this to get the tissue name
    tissues = [x.split(":")[1] for x in shared]
    pairs_of_tissues = list(itertools.combinations(tissues, 2))
    #print "Tissues ", tissues, pairs_of_tissues
    add_pairs_to_tissue_matrix(pairs_of_tissues)

def print_mutations_shared_tissues_across_individuals():
    for locus in shared_among_tissues:
        if(len(shared_among_tissues[locus]) > 1):
            shared = shared_among_tissues[locus]
            #print "shared among tissues", locus, shared
            add_to_tissue_matrix(shared)

def main():
    vcf_f = sys.argv[1]
    read_vcf(vcf_f)
    #print_samples_with_mutations_shared_tissues() #in the same individual, these are mostly just duplicates in the input
    print_mutations_shared_tissues_across_individuals()
    print_tissue_matrix()

if __name__ == "__main__":
    main()
