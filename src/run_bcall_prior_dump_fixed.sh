#The first argument is the sample-subset, the second argument is the dump-output-file

#initial - For all alts
LD_LIBRARY_PATH=/gscmnt/gc2607/mardiswilsonlab/aramu/src/gcc-5.2.0/build/lib64/:$LD_LIBRARY_PATH \
    ../bin/bcall-0.1.1 prior-dump-fixed $1 $2 ../dat/SeqCap_EZ_Exome_v3_primary.bed.gz

#For non-zero alts
LD_LIBRARY_PATH=/gscmnt/gc2607/mardiswilsonlab/aramu/src/gcc-5.2.0/build/lib64/:$LD_LIBRARY_PATH \
    ../bin/bcall-nonzeroalt prior-dump-fixed $1 $2 ../dat/SeqCap_EZ_Exome_v3_primary.bed.gz
