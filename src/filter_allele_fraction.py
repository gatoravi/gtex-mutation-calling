import vcf

vcf_reader = vcf.VCFReader(open('samtools1.1_output.vcf'))
vcf_writer = vcf.Writer(open('/dev/stdout', 'w'), vcf_reader)

for rec in vcf_reader:
    DP4 = rec.samples[0]['DP4']
    ref_total = float(DP4[0] + DP4[1])
    alt_total = float(DP4[2] + DP4[3])
    AF = alt_total/(ref_total + alt_total)
    if AF > 0.01:
        rec.INFO['AF'] = AF
        vcf_writer.write_record(rec)
