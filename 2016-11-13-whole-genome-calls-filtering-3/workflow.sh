rm -f $BASEDIR/all_calls_sorted_filtered*

#Filter on variant supporting read-count
zcat < $BASEDIR/all_calls_sorted_v2.tsv.gz | awk '$6 > 5' > $BASEDIR/all_calls_sorted_filtered_1.tsv

#Convert 1-based to BED format
awk '{ printf($1"\t"$2-1"\t"$2); for (i = 3; i <= NF; i++) { printf("\t"$i); } printf("\n"); }'  $BASEDIR/all_calls_sorted_filtered_1.tsv > $BASEDIR/all_calls_sorted_filtered_1.bed

#Make the BED the TSV
mv $BASEDIR/all_calls_sorted_filtered_1.bed $BASEDIR/all_calls_sorted_filtered_1.tsv
bgzip $BASEDIR/all_calls_sorted_filtered_1.tsv

#Filter out RNA editing sites
bedtools intersect -a <(zcat $BASEDIR/all_calls_sorted_filtered_1.tsv.gz) -b ~/dat/rna-editing/Human_AG_all_hg19_v2.bed -wa -v | bgzip > $BASEDIR/all_calls_sorted_filtered_2.tsv.gz

#Identify sites that don't overlap with exome SNP calls
bedtools intersect -a <(zcat $BASEDIR/all_calls_sorted_filtered_2.tsv.gz) -b <(zcat $BASEDIR/../dat/GTEx_Analysis_20150112_ExomeSeq_520Indiv_GATK_HaplotypeCaller_SNPs_INDELs.bed.gz) -wa -v | bgzip > $BASEDIR/all_calls_sorted_filtered_3.tsv.gz

#Identify sites that don't overlap with WGS SNP calls
bedtools intersect -a <(zcat $BASEDIR/all_calls_sorted_filtered_3.tsv.gz) -b <(zcat $BASEDIR/../dat/GTEx_Analysis_20150112_WholeGenomeSeq_VarSitesAnnot.bed.gz) -wa -v | bgzip > $BASEDIR/all_calls_sorted_filtered_4.tsv.gz

#Filter based on individual cumul vaf using a binomial test
gtexmc filter-individual-cumulative-vaf-binomialtest <(zcat $BASEDIR/all_calls_sorted_filtered_4.tsv.gz) $BASEDIR/../dat/SRARunTable.txt 2>$BASEDIR/cumulvaf-binomial-filteredout.tsv | vcf-sort | bgzip >  $BASEDIR/all_calls_sorted_filtered_5.tsv.gz

#Filter on total readcount
awk '$6 + $7 > 20' <(zcat $BASEDIR/all_calls_sorted_filtered_5.tsv.gz) > $BASEDIR/all_calls_sorted_filtered_6.tsv
bgzip $BASEDIR/all_calls_sorted_filtered_6.tsv

#Add tissue,subject information to bcall results
gtexmc add-tissue-subject-to-calls <(zcat $BASEDIR/all_calls_sorted_filtered_6.tsv.gz) $BASEDIR/../dat/SRARunTable.txt | vcf-sort > $BASEDIR/all_calls_sorted_filtered_7.tsv
bgzip $BASEDIR/all_calls_sorted_filtered_7.tsv

#Get the alternate-allele
gtexmc genotyper <(zcat $BASEDIR/all_calls_sorted_filtered_7.tsv.gz) > $BASEDIR/all_calls_sorted_filtered_8.tsv
bgzip $BASEDIR/all_calls_sorted_filtered_8.tsv

#Filter out sites which contain read support for more than two alleles - greater than 2 reads for more than one alternate allele gets filtered out
gtexmc filter-max-variant-alleles <(zcat $BASEDIR/all_calls_sorted_filtered_8.tsv.gz) 2>$BASEDIR/filtered-out-max-variant-alleles > $BASEDIR/all_calls_sorted_filtered_9.tsv
bgzip $BASEDIR/all_calls_sorted_filtered_9.tsv

#Filter using VDB cutoff of 0.1 and with read support on both strands.
gtexmc filter-using-vdb-and-strands <(zcat $BASEDIR/../2016-10-03-filter-using-VDB/bcall_vdb_metrics_v3.tsv.gz) <(zcat $BASEDIR/all_calls_sorted_filtered_9.tsv.gz) 0.1 >$BASEDIR/all_calls_sorted_filtered_10.tsv 2>$BASEDIR/vdb_not_found.tsv
bgzip $BASEDIR/all_calls_sorted_filtered_10.tsv

#Remove calls from samples flagged by GTEx
gtexmc filter-flagged-samples ../dat/flagged_samples.tsv <(zcat $BASEDIR/all_calls_sorted_filtered_10.tsv.gz) 2>/dev/null  | bgzip > $BASEDIR/all_calls_sorted_filtered_11.tsv.gz

#Filter out putative rna-editing sites
awk -F"\t" '{ print $1"\t"$3"\t"$3"\t"$4"\t"$5"\t"$(NF-4)"\t"$NF }' <(zcat all_calls_sorted_filtered_11.tsv.gz) > $BASEDIR/all_calls_sorted_filtered_11.avinput
annotate_variation.pl --geneanno -buildver hg19 $BASEDIR/all_calls_sorted_filtered_11.avinput -dbtype wgEncodeGencodeBasicV19 ~/dat/annovar/humandb/ --outfile $BASEDIR/all_calls_sorted_filtered_11.avoutput
gtexmc filter-rnaediting  <(zcat $BASEDIR/all_calls_sorted_filtered_11.tsv.gz) $BASEDIR/all_calls_sorted_filtered_11.avoutput.variant_function ~/dat/annovar/humandb/hg19_wgEncodeGencodeBasicV19.genes_strands.tsv | bgzip > $BASEDIR/all_calls_sorted_filtered_12.tsv.gz
