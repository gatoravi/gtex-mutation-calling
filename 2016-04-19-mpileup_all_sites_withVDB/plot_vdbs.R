require(ggplot2)
t<-read.table("all_vdbs.txt")
ggplot(t) + geom_bar(aes(x = V1))
ggsave("VDB_all_except_inf.pdf")
t1 <- as.data.frame(t[t$V1>0.1,])
colnames(t1) <- "V1"
ggplot(t1) + geom_bar(aes(x = V1))
ggsave("VDB_greaterthan0_015.pdf")
t2 <- as.data.frame(t[t$V1>0.015,])
colnames(t2) <- "V1"
ggplot(t2) + geom_bar(aes(x = V1))
ggsave("VDB_greaterthan0_015.pdf")
