#We aligned the RNA-seq reads to the MM9/NCBIM37 reference genome with BWA v0.5.5 [29] and initially called SNVs with samtools/bcftools v0.1.16 (r963:234) [15] (Figure s5 in Additional file 2). The reads from candidate sites were realigned in a splice-aware manner using exonerate v2.4.0 with parameters: -m e2g -n 2 -Q dna -T dna --wordjump 20 -s 300 --fsmmemory 1024 --saturatethreshold 100 [16]. The End Distance Bias and Strand Bias filters of samtools/bcftools were set to 0.05 and 0.01. While End Distance Bias checks if variant bases tend to occur at a fixed distance from the end of reads using a t-test, the Variant Distance Bias evaluates the likelihood of the mean pairwise distance of the variant bases in the aligned portion of the reads. The Variant Distance Bias filter (Figure s4 in Additional file 2) was set to 0.015 and this filtering method was contributed to the main publicly available development branch of samtools/bcftools.

/*
 *  calc_vdb() - returns value between zero (most biased) and one (no bias)
 *               on success, or HUGE_VAL when VDB cannot be calculated because
 *               of insufficient depth (<2x)
 *
 *  Variant Distance Bias tests if the variant bases are positioned within the
 *  reads with sufficient randomness. Unlike other tests, it looks only at
 *  variant reads and therefore gives different kind of information than Read
 *  Position Bias for instance. VDB was developed for detecting artefacts in
 *  RNA-seq calls where reads from spliced transcripts span splice site
 *  boundaries.  The current implementation differs somewhat from the original
 *  version described in supplementary material of PMID:22524474, but the idea
 *  remains the same. (Here the random variable tested is the average distance
 *  from the averaged position, not the average pairwise distance.)
 *
 *  For coverage of 2x, the calculation is exact but is approximated for the
 *  rest. The result is most accurate between 4-200x. For 3x or >200x, the
 *  reported values are slightly more favourable than those of a true random
 *  distribution.
 */

bsub -oo mpileup_1.oo 'bash run_mpileup_1.sh'
bsub -oo mpileup_2.oo 'bash run_mpileup_2.sh'

grep -v '#' samtools1.1_output.vcf | wc -l
 14084
awk '{ gsub(";", "\t"); print }' samtools1.1_output.vcf  | grep -oP 'VDB=.*' | cut -f1  | sed -ne 's/VDB=//p' | grep -v inf | awk '$1>0.01' | wc -l
 7679 (54.52% of original sites are retained.)
python count_end_mismatches.py samtools1.1_output.txt | awk '$6>0.5' | wc -l
scp aramu@clia1:/opt/gms/aramu/test/samtools1.1_output_annotated.vcf .

 ##INFO=<ID=INDEL,Number=0,Type=Flag,Description="Indicates that the variant is an INDEL.">
 ###INFO=<ID=IDV,Number=1,Type=Integer,Description="Maximum number of reads supporting an indel">
 ###INFO=<ID=IMF,Number=1,Type=Float,Description="Maximum fraction of reads supporting an indel">
 ###INFO=<ID=DP,Number=1,Type=Integer,Description="Raw read depth">
 ###INFO=<ID=VDB,Number=1,Type=Float,Description="Variant Distance Bias for filtering splice-site artefacts in RNA-seq data (bigger is better)",Version="3">
 ###INFO=<ID=RPB,Number=1,Type=Float,Description="Mann-Whitney U test of Read Position Bias (bigger is better)">
 ###INFO=<ID=MQB,Number=1,Type=Float,Description="Mann-Whitney U test of Mapping Quality Bias (bigger is better)">
 ###INFO=<ID=BQB,Number=1,Type=Float,Description="Mann-Whitney U test of Base Quality Bias (bigger is better)">
 ###INFO=<ID=MQSB,Number=1,Type=Float,Description="Mann-Whitney U test of Mapping Quality vs Strand Bias (bigger is better)">
 ###INFO=<ID=SGB,Number=1,Type=Float,Description="Segregation based metric.">
 ###INFO=<ID=MQ0F,Number=1,Type=Float,Description="Fraction of MQ0 reads (smaller is better)">
 ###INFO=<ID=I16,Number=16,Type=Float,Description="Auxiliary tag used for calling, see description of bcf_callret1_t in bam2bcf.h">
 ###INFO=<ID=QS,Number=R,Type=Float,Description="Auxiliary tag used for calling">
 #
 
vcflib can be used for filtering:
Below, I show that the results of filtering are same from using awk, vcffilter.
aramu@Avinashs-MacBook-Pro ~/src/vcflib (master)> ./bin/vcffilter -f 'VDB > 0.015' ./samtools1.1_output_annotated_noinf.vcf  2>&1  | grep -v '#' | wc -l
    7536
aramu@Avinashs-MacBook-Pro ~/src/vcflib (master)>  awk '{ gsub(";", "\t"); print }'  samtools1.1_output_annotated_noinf.vcf | grep -o 'VDB=.*' | cut -f1  | sed -ne 's/VDB=//p' | grep -v inf | awk '$1 > 0.015'  | wc -l
    7536
awk '{ gsub(";", "\t"); print }'  samtools1.1_output_annotated_noinf.vcf | grep -o 'VDB=.*' | cut -f1  | sed -ne 's/VDB=//p' | grep -v inf | awk '$1 > 0.1'  | wc -l
    6111

Looks like there are some duplicates in the mpileup VCF, 93 duplicates, need to figure
out why this is!
wc -l ../dat/all.bscore.VAF10.out
 14020 ../dat/all.bscore.VAF10.out
sort -u ../dat/all.bscore.VAF10.out | wc -l
 14020
grep -v '#' samtools1.1_output.vcf | wc -l
 14084
grep -v '#' samtools1.1_output.vcf  | sort -u | wc -l
 13991
We are also missing 29 variants in the mpileup vcf.
