rm -f  samtools1.1_output.txt
awk '{
    sub("chr", "", $1);
    print "/gscuser/aramu/src/samtools/samtools mpileup -O -f /gscmnt/gc2607/mardiswilsonlab/aramu/dat/hs37/all_sequences.fa /gscmnt/gc2802/halllab/cchiang/gtex/rna-seq/4101/bam/"$6".bam -r "$1":"$2"-"$2 " >> samtools1.1_output.txt"; \
}' <(cat /gscmnt/gc2607/mardiswilsonlab/aramu/test/gtex/dat/all.bscore.VAF10.out) | bash
