Andrew's email to Don with column explanations.

"
Good evening Don,
 
I think it worked. I had to rewrite a bunch of the analysis software to reduce the memory, disk and cpu footprint. The cluster was unforgiving going from 80 samples to 10,000 samples. I had to simplify the error model to get it all to fit on disk. Instead of making a different error model for each sample that excludes samples from the same individual, I made just one model for the likelihood of calling a substitution at a position given the observed substitution rate in all samples. In theory this shouldn’t make a difference because of the sheer number of samples.
 
I used the following criteria to call variants/exclude data from analysis:
 
1.       Minimum coverage at a position needed for inclusion in the analysis: 20
2.       Minimum reads calling a variant to report the variant: 2
3.       Minimum p-value (Bonferroni corrected n=9,303,093,480): 0.05
4.       Minimum VAF: 0.0001
 
I included the final output file with all of the variant calls here: /scratch/tdlab/alyoung/shared/all.bscore.out. The columns are formatted as follows:
 
1.       Chromosome
2.       Position (1-based)
3.       Substitution
4.       Reads supporting variant
5.       Total reads sequenced
6.       Colon delimited sample ID, individual ID, tissue type
7.       Aggregate substitution rate from the entire cohort (excluding samples with less than 20x coverage).
8.       Binomial p-value of observing that VAF given the distribution
 
Even though I sent you the file, I couldn’t resist making a few plots to summarize the findings. I attached a slide deck of the figures and will describe them below.
 
Using the above criteria to call variants, there were roughly 1.8M variants present. Slide 1 depicts the histogram of VAF measured for all of the 1.8M variants that passed the binomial calling gauntlet.
 
Interestingly, I noticed that many of the variants called were on chrM. I don’t know if this is a signature of really cool biology or just artifact. Slide 2 depicts the same histogram after excluding the variants called on chrM. It’s crazy that tons of the variants called at the low end of the frequency spectrum were called from chrM. I think it’s also cool that the binomial model is calling a ton of heterozygous and homozygous variants. These are probably germline variants that are rare/private in the cohort so they don’t increase the mean frequency of that substitution overall.
 
So, this is where things get pretty cool. Once you exclude the chrM data, there are 880,352 variants called. When you additionally filter to exclude variants present above 0.10 VAF, there are only 14,020 variants called. The frequency distribution of these low frequency variants are plotted in slide 3.  A VAF cutoff of 0.1 might be a little too aggressive. There are most likely subclonal populations hanging out in some of these tissues around 0.2 or 0.3 VAF. However, based on the first two histograms the bulk of the non-germline datapoints are below 0.1 VAF. But, all of the variants are present in the file I sent you, so these higher frequency variants/subclones can be analyzed later.
 
Next, I plotted the total number of subclonal variants (<0.1 VAF, no chrM) called by tissue type (slide 4). It seems like there are a lot of subclones visible in whole blood and thyroid. However, I realized that this plot did not account for the number of samples analyzed for each tissue type. Slide 5 plots the number of variants called for each tissue type divided by the number of samples analyzed for each tissue type. Again, whole blood and thyroid stand out. But, interestingly, liver also tops the chart now. This makes sense because 727 rare variants were identified (not on chrM and VAF <0.1) in 116 samples for liver, compared to 2193 rare variants in 308 samples for whole blood.
 
Slide 6 is a box and whiskers plot depicting the number of variants identified by sample and organized by tissue type. Slide 6 depicts the entire y-axis with all of the outliers. Slide 7 limits the y-axis to 0-20 variants per sample. As you can see most of the samples have no variants called. But, the tail is long. In aggregate there are 2,994 samples where a variant was called below 0.1 VAF (and not on chrM).
 
There is a bunch of data here. I’d be happy to sit down with you and go through the raw data or the figures. Just let me know what you want to do.
 
Of course, none of these results include what will be the most interesting finding… what genes these mutations are in. I am kind of swamped right now trying to write a manuscript before the baby comes, so I don’t have a lot of time to annotate the variants. However, I think once you have the variants in hand it should be pretty straightforward to annotate them. Regardless, I made one quick look at the results from whole blood. I just sorted the rare variants by position and it seems like there are positions with subclonal mutations called in multiple samples from different individuals. This could be real biology or position specific errors that are still significant despite the binomial cutoffs. Slide 8 contains one example of what I am talking about. At this position there are 16 samples (out of the ~300 total) that call a G to A substitution at a rate outside of what’s expected based on the binomial distribution. When you look at this position into UCSC, it’s a predicated stop gain in SDHB. It’s also a COSMIC hit (COSM677083) that’s been seen previously in 1 case of lung cancer. This might just be coincidence, but, regardless, I think there is a treasure trove of information here. I am excited to see what findings come from these results.
 
Please let me know if you want to chat about these finding. This is very exciting. Also, please spot check/double check my findings. I included the aggregate frequency for each substitution in the output. So, it should be easy to go back into the raw data and see what the aggregate substitution rate is for samples with >=20x coverage at that position.
 
Have a good night.
 
Cheers,
Andrew
"

Download RNA editing sites:
wget http://web.stanford.edu/~gokulr/database/Human_AG_all_hg19_v2.txt


The initial gtexmc was done on 183 cancer genes(and MTGenome)
aramu@linus217 /gscmnt/gc2719/halllab/users/cchiang/projects/gtex/rna_editing_2015-07-10/run_2015-10-14>
 cut -f3  Cancer_gene_sites_for_testing.txt | sort -u | wc -l
183

Initial mpileup from Colby is on disk here - /gscmnt/gc2719/halllab/users/cchiang/projects/gtex/rna_editing_2015-07-10/run_2015-10-14
It consists of a 25GB archive with readcounts for all samples.

The list of all samples is in samplelist.txt in this folder:
wc -l samplelist.txt
9597 samplelist.txt

Use this FASTA, same as the previous analysis,
/gscmnt/gc2719/halllab/users/cchiang/projects/gtex/rna_editing_2015-07-10/Homo_sapiens_assembly19.fasta

#The flagged samples were obtained from the GTEx spreadsheet, these were parsed out to obtain GTEx SRR ids
#Don and I discussed which flagging criteria to retain and which to exclude this information is in flagged_criteria.tsv
python  ../src/get-flagged-samples.py > flagged_samples.tsv

#Plot number of samples per tissue
t <- read.table("SRARunTable.txt", head = T, sep = "\t")
reorder_size <- function(x) {
       factor(x, levels = names(sort(table(x))))
     }
ggplot(t) + geom_bar(aes(x = reorder_size(body_site_s))) + theme(axis.text.x = element_text(angle=90))
ggsave("ntissue_distbn.pdf")
#Get the non-flagged samples:
grep -v -f <(cut -f2 flagged_lowrin_samples.tsv) samplelist.txt > notflagged_samples.tsv
