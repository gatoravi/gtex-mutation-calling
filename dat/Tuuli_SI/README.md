"Here is the file containing the clonality statistic from Tuuli - not sure if I shared this yet or not
[10:46] 
monoall_X_perc is the column of interest  - it scales between 0 and 1 - 1 is completely mono allelic (clonal)"

"these are het SNP’s and we are trying to look for selection for one of the alleles in a majority of the SNP’s .. so samples with this stat close to 1 are clonal"
