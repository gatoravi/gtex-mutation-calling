#The file with the list of calls is $1 - Assumes BED format
#Uses samtools1.1
rm -f  samtools1.1_output.vcf
awk '{
    sub("chr", "", $1);
    print "samtools1.1 mpileup -uv -f /gscmnt/gc2607/mardiswilsonlab/aramu/dat/hs37/all_sequences.fa /gscmnt/gc2802/halllab/cchiang/gtex/rna-seq/4101/bam/"$18".bam -r "$1":"$2"-"$2 " | grep -v \"#\" >> samtools1.1_output.vcf"; \
}' <(zcat $1) | bash
