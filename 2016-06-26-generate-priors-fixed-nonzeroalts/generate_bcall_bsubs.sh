for i in 0{0..9} {10..95}
do
    #To generate the prior-dump commands
    echo "bsub -N -R \"select[mem>16000] rusage[mem=16000]\" -M 16000000 -o log/subset"$i".o -e log/subset"$i".e" \
         " -g /aramu/bcall bash ../src/run_bcall_prior_dump_fixed.sh sample_subsets/subset"$i" ./prior_dumps/subset"$i".dump"
    #To generate sample list for prior-merge
    #echo "subset"$i" ./prior_dumps/subset"$i".dump"
done
