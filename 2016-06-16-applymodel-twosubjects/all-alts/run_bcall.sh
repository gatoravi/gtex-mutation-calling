#! /usr/bin/env bash

subject=$1

rm -f ${subject}.[eo] && bsub -N -e ${subject}.e  -o ${subject}.o  -R "select[mem>8000] rusage[mem=8000]" -M 8000000 "LD_LIBRARY_PATH=/gscmnt/gc2607/mardiswilsonlab/aramu/src/gcc-5.2.0/build/lib64/:$LD_LIBRARY_PATH ../bin/bcall-0.3.1 call-using-merged ${subject}_readcount_files.tsv /gscmnt/gc2607/mardiswilsonlab/aramu/test/gtex/2016-06-05-generate-priors-fixed/prior_dumps/merged.dump && rm -f ${subject}_calls.tsv && vcf-sort ${subject}.o  > ${subject}_calls.tsv && rm -f ${subject}_calls_n1.tsv && gtexmc filter-shared-across-samples ${subject}_calls.tsv  1 | vcf-sort  > ${subject}_calls_n1.tsv"
