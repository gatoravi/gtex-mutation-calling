Reading merged dump /gscmnt/gc2607/mardiswilsonlab/aramu/test/gtex/2016-06-05-generate-priors-fixed/prior_dumps/merged.dump

Applying model to SRR1499652
Opening /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1499652_readcounts.txt.gz
Read 40063921 lines from /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1499652_readcounts.txt.gz
The number of tests performed for this sample is: 140045
Applying the BH-procedure to control FDR.
The p-value cutoff is:5.20996e-06
The number of sites not in the region of interest is 18538886
Applying model to SRR1466025
Opening /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1466025_readcounts.txt.gz
Read 29535696 lines from /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1466025_readcounts.txt.gz
The number of tests performed for this sample is: 191290
Applying the BH-procedure to control FDR.
The p-value cutoff is:5.20996e-06
The number of sites not in the region of interest is 12849423
Applying model to SRR1454310
Opening /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1454310_readcounts.txt.gz
Read 28267309 lines from /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1454310_readcounts.txt.gz
The number of tests performed for this sample is: 164902
Applying the BH-procedure to control FDR.
The p-value cutoff is:5.20996e-06
The number of sites not in the region of interest is 11565968
Applying model to SRR1419211
Opening /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1419211_readcounts.txt.gz
Read 46519410 lines from /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1419211_readcounts.txt.gz
The number of tests performed for this sample is: 124788
Applying the BH-procedure to control FDR.
The p-value cutoff is:5.20996e-06
The number of sites not in the region of interest is 25121200
Applying model to SRR1353499
Opening /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1353499_readcounts.txt.gz
Read 32681576 lines from /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1353499_readcounts.txt.gz
The number of tests performed for this sample is: 99010
Applying the BH-procedure to control FDR.
The p-value cutoff is:5.20996e-06
The number of sites not in the region of interest is 15664485
Applying model to SRR1334791
Opening /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1334791_readcounts.txt.gz
Read 38637477 lines from /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1334791_readcounts.txt.gz
The number of tests performed for this sample is: 112419
Applying the BH-procedure to control FDR.
The p-value cutoff is:5.20996e-06
The number of sites not in the region of interest is 18497071
Applying model to SRR1351857
Opening /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1351857_readcounts.txt.gz
Read 39430800 lines from /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1351857_readcounts.txt.gz
The number of tests performed for this sample is: 133503
Applying the BH-procedure to control FDR.
The p-value cutoff is:5.20996e-06
The number of sites not in the region of interest is 21297402
Applying model to SRR1330427
Opening /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1330427_readcounts.txt.gz
Read 36349056 lines from /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1330427_readcounts.txt.gz
The number of tests performed for this sample is: 140805
Applying the BH-procedure to control FDR.
The p-value cutoff is:5.20996e-06
The number of sites not in the region of interest is 15857966
Applying model to SRR1329400
Opening /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1329400_readcounts.txt.gz
Read 41779446 lines from /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1329400_readcounts.txt.gz
The number of tests performed for this sample is: 152691
Applying the BH-procedure to control FDR.
The p-value cutoff is:5.20996e-06
The number of sites not in the region of interest is 20751591
Applying model to SRR1400507
Opening /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1400507_readcounts.txt.gz
Read 39230602 lines from /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1400507_readcounts.txt.gz
The number of tests performed for this sample is: 138390
Applying the BH-procedure to control FDR.
The p-value cutoff is:5.20996e-06
The number of sites not in the region of interest is 17817651
Applying model to SRR1470855
Opening /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1470855_readcounts.txt.gz
Read 36893656 lines from /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1470855_readcounts.txt.gz
The number of tests performed for this sample is: 119587
Applying the BH-procedure to control FDR.
The p-value cutoff is:5.20996e-06
The number of sites not in the region of interest is 17193986
Applying model to SRR1333570
Opening /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1333570_readcounts.txt.gz
Read 28324282 lines from /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1333570_readcounts.txt.gz
The number of tests performed for this sample is: 108873
Applying the BH-procedure to control FDR.
The p-value cutoff is:5.20996e-06
The number of sites not in the region of interest is 14662317
Applying model to SRR1410493
Opening /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1410493_readcounts.txt.gz
Read 47064870 lines from /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1410493_readcounts.txt.gz
The number of tests performed for this sample is: 114606
Applying the BH-procedure to control FDR.
The p-value cutoff is:5.20996e-06
The number of sites not in the region of interest is 26160640
Applying model to SRR1381306
Opening /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1381306_readcounts.txt.gz
Read 41898465 lines from /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1381306_readcounts.txt.gz
The number of tests performed for this sample is: 226780
Applying the BH-procedure to control FDR.
The p-value cutoff is:5.20996e-06
The number of sites not in the region of interest is 19555272
Applying model to SRR1417088
Opening /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1417088_readcounts.txt.gz
Read 40725195 lines from /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1417088_readcounts.txt.gz
The number of tests performed for this sample is: 139349
Applying the BH-procedure to control FDR.
The p-value cutoff is:5.20996e-06
The number of sites not in the region of interest is 19356910