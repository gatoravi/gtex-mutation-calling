#! /usr/bin/bash
# ARG1 is the region
tabix /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1329400_readcounts.txt.gz $1
tabix /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1330427_readcounts.txt.gz $1
tabix /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1333570_readcounts.txt.gz $1
tabix /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1334791_readcounts.txt.gz $1
tabix /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1351857_readcounts.txt.gz $1
tabix /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1353499_readcounts.txt.gz $1
tabix /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1381306_readcounts.txt.gz $1
tabix /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1400507_readcounts.txt.gz $1
tabix /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1410493_readcounts.txt.gz $1
tabix /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1417088_readcounts.txt.gz $1
tabix /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1419211_readcounts.txt.gz $1
tabix /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1454310_readcounts.txt.gz $1
tabix /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1466025_readcounts.txt.gz $1
tabix /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1470855_readcounts.txt.gz $1
tabix /gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/SRR1499652_readcounts.txt.gz $1
