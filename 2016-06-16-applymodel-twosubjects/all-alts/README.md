```bash
grep '^SRR1317470' merged-call.oo   | wc -l
 792168
grep '^SRR1391748' merged-call.oo   | wc -l
 740233
```

```perl
> SRR1317470 <- read.table("SRR1317470_ref_alt.tsv")
> nrow(subset(SRR1317470, V2/(V2+V1) > 0.1))
[1] 8510
> nrow(SRR1317470)
[1] 792168
> nrow(subset(SRR1317470, V2/(V2+V1) > 0.2))
[1] 6946
> nrow(subset(SRR1317470, V2/(V2+V1) > 0.3))
[1] 6597
> nrow(subset(SRR1317470, V2/(V2+V1) > 0.01))
[1] 403007
> nrow(subset(SRR1317470, V2/(V2+V1) > 0.05))
[1] 53215
> SRR1391748 <- read.table("SRR1391748_ref_alt.tsv")
> nrow(SRR1391748)
[1] 740233
> nrow(subset(SRR1317470, V2/(V2+V1) > 0.2))
[1] 6946
> nrow(subset(SRR1317470, V2/(V2+V1) > 0.3))
[1] 6597
> nrow(subset(SRR1317470, V2/(V2+V1) > 0.01))
[1] 403007
> nrow(subset(SRR1317470, V2/(V2+V1) > 0.05))
[1] 53215
> nrow(subset(SRR1317470, V2/(V2+V1) < 0.2))
[1] 785209
> nrow(subset(SRR1317470, V2/(V2+V1) < 0.2 &  V2/(V2+V1) > 0.05))
[1] 46256
```

```bash
 sed -ne '178,179p' subject_to_samples.tsv
 GTEX-12KS4 - 15 samples
SRR1314976
SRR1331922
SRR1357692
SRR1360785
SRR1360828
SRR1365270
SRR1370308
SRR1405461
SRR1416495
SRR1417851
SRR1432253
SRR1435833
SRR1447547
SRR1480941
SRR1498595

GTEX-12WSJ - 15 samples
SRR1329400
SRR1330427
SRR1333570
SRR1334791
SRR1351857
SRR1353499
SRR1381306
SRR1400507
SRR1410493
SRR1417088
SRR1419211
SRR1454310
SRR1466025
SRR1470855
SRR1499652
```

## Inputs to bcall
awk '{ print $1"\t/gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/"$1"_readcounts.txt.gz" }' GTEX-12KS4_readcount_files.tsv  > t; mv t GTEX-12KS4_readcount_files.tsv
awk '{ print $1"\t/gscmnt/gc2802/halllab/aramu/gtex-readcounts/2016-05-27/"$1"_readcounts.txt.gz" }' GTEX-12WSJ_readcount_files.tsv  > t; mv t GTEX-12WSJ_readcount_files.tsv

- Figure out a set of samples to run bcall on:
        - gtexmc subject-to-samples ../../../dat/SRARunTable.txt ../../../dat/samplelist.txt > subject_to_samples.tsv


## Run bcall
bash run_bcall.sh GTEX-12WSJ
bash run_bcall.sh GTEX-12KS4
sed -ne '85,86p' README.md  | bash
rm -f GTEX-12WSJ_calls.tsv; vcf-sort GTEX-12WSJ.o  > GTEX-12WSJ_calls.tsv
rm -f GTEX-12WSJ_calls_n1.tsv; gtexmc filter-shared-across-samples GTEX-12WSJ_calls.tsv  1 | vcf-sort  > GTEX-12WSJ_calls_n1.tsv
