#The raw calls from bcall(after applying Bonferroni correction) are here, all_calls_sorted.tsv.gz
#This is the starting point for all analysis.


Compare the effects of two kinds of filtering:
1. Filter based on background allele frequency at a locus (try cutoffs of 0.01 and 0.05)
2. Filter based on cumulative VAF at a locus in each individual.

Use the all_calls_sorted_filtered_4.tsv as the starting point.
Compare how many calls are filtered out to the number from filtering out
sites shared across samples of an individual.

#Preliminary analysis - compare filtering based on background vs filtering based on sharing amongst tissues
11,007,040 calls in ../2016-08-18-whole-genome-calls-filtering/all_calls_sorted_filtered_4.tsv
#5 percent cutoff
awk '$15/($15 + $14) < 0.05'  ../2016-08-18-whole-genome-calls-filtering/all_calls_sorted_filtered_4.tsv  | wc -l
  3,852,372
#1 percent cutoff
awk '$15/($15 + $14) < 0.01'  ../2016-08-18-whole-genome-calls-filtering/all_calls_sorted_filtered_4.tsv  | wc -l
  2,682,482
#Filtering calls shared amongst tissues of a individual.
wc -l ../2016-08-18-whole-genome-calls-filtering/all_calls_sorted_filtered_5.tsv
  937,181 ../2016-08-18-whole-genome-calls-filtering/all_calls_sorted_filtered_5.tsv

wc -l all_calls_sorted_filtered_*tsv
67,312,945 all_calls_sorted_filtered_1.tsv
45,234,285 all_calls_sorted_filtered_2.tsv
34,465,572 all_calls_sorted_filtered_3.tsv
7,154,776 all_calls_sorted_filtered_4.tsv
7,154,776 all_calls_sorted_filtered_5.tsv
7,154,776 all_calls_sorted_filtered_6.tsv
7,119,798 all_calls_sorted_filtered_7.tsv

#Number of sites below VAF 0.375
awk '$8/($7 + $8) < 0.375' all_calls_sorted_filtered_7.tsv | wc -l
 1,657,801
awk '{ print $1":"$2 }'  all_calls_sorted_filtered_7.tsv | sort -u | wc -l
  561880


##Quantify locus sharing
    awk '{ print $1":"$2 }'  all_calls_sorted_filtered_8.tsv | sort | uniq -c | sort -nrk1,1 > locus_counts.tsv
> ggplot(locus_counts) + geom_histogram(aes(x = V1)) + xlab("Number of tissues sharing a loci") + scale_x_log10()
`stat_bin()` using `bins = 30`. Pick better value with `binwidth`.
> ggsave("tissue_sharing.pdf")
#Tabix index the calls
~/bin/bgzip all_calls_sorted_filtered_1.tsv
tabix -s 1 -b 2 -e 3 all_calls_sorted_filtered_1.tsv.gz
~/bin/bgzip all_calls_sorted_filtered_2.tsv
tabix -s 1 -b 2 -e 3 all_calls_sorted_filtered_2.tsv.gz
~/bin/bgzip all_calls_sorted_filtered_3.tsv
tabix -s 1 -b 2 -e 3 all_calls_sorted_filtered_3.tsv.gz
~/bin/bgzip all_calls_sorted_filtered_4.tsv
tabix -s 1 -b 2 -e 3 all_calls_sorted_filtered_4.tsv.gz
~/bin/bgzip all_calls_sorted_filtered_5.tsv
tabix -s 1 -b 2 -e 3 all_calls_sorted_filtered_5.tsv.gz
~/bin/bgzip all_calls_sorted_filtered_6.tsv
tabix -s 1 -b 2 -e 3 all_calls_sorted_filtered_6.tsv.gz
~/bin/bgzip all_calls_sorted_filtered_7.tsv
tabix -s 1 -b 2 -e 3 all_calls_sorted_filtered_7.tsv.gz
~/bin/bgzip all_calls_sorted_filtered_8.tsv
tabix -s 1 -b 2 -e 3 all_calls_sorted_filtered_8.tsv.gz
~/bin/bgzip all_calls_sorted_filtered_9.tsv
tabix -s 1 -b 2 -e 3 all_calls_sorted_filtered_9.tsv.gz
~/bin/bgzip all_calls_sorted_filtered_10.tsv
tabix -s 1 -b 2 -e 3 all_calls_sorted_filtered_10.tsv.gz
tabix -s 1 -b 2 -e 3 all_calls_sorted_filtered_11.tsv.gz
tabix -s 1 -b 2 -e 3 all_calls_sorted_filtered_2.tsv.gz
