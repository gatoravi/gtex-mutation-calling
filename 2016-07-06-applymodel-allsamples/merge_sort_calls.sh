cat calls/subset*_bcall.tsv | grep -v chr | vcf-sort | bgzip > all_calls_sorted.tsv.gz
tabix -s 1 -b 2 -e 2 all_calls_sorted.tsv.gz
cat calls/subset*_bcall.tsv | wc -l
