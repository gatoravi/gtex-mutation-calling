t<-read.table("ka_ks.tsv", head = T)
require(ggplot2)
ggplot(t) + geom_histogram(aes(x = ka_ks))
ggsave("ka_ks_histogram.pdf")
