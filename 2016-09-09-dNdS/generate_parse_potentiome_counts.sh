#! /usr/bin/env bash
for file in /scratch/dclab/potentiome/out/synthesized.snps.*
do
    out=${file##*/}
    echo "python3 ~/src/gtex-mutation-calling/src/dNdS/parse_potentiome.py " $file " > "$out"_counts.tsv"
done
