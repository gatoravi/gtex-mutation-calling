#! /usr/bin/env python
import sys
import csv

def usage():
    print(sys.argv[0] + " sample_nmuts annovar_exonic_var_file nmuts_cutoff")
    #../2016-05-09-SI_statistic_num_mutations/sample_nmuts_SI_v11.tsv 
    # sample subject tissue sex age total_n_mutations SI_metric

#Key is sample with high mutants, val is 1
high_mutants = {}
def read_high_mutants(sample_nmuts, cutoff):
    with open(sample_nmuts) as snfh:
        snfhc = csv.DictReader(snfh, delimiter = "\t")
        for line in snfhc:
            if int(line['total_n_mutations']) > cutoff:
                high_mutants[line['sample']] = 1

#Only print annovar op of samples which are not high mutant
def filter_high_mutants(annovar_op):
    with open(annovar_op) as afh:
        for line in afh:
            line = line.rstrip("\n")
            fields = line.split("\t")
            sample = fields[-1]
            if sample not in high_mutants:
                print(line)

def main():
    if(len(sys.argv) < 4):
        return usage()
    sample_nmuts = sys.argv[1]
    annovar_op = sys.argv[2]
    cutoff = int(sys.argv[3])
    read_high_mutants(sample_nmuts, cutoff)
    filter_high_mutants(annovar_op)

main()
