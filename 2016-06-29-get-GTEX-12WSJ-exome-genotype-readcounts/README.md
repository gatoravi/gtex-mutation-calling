## From Don
I like your point about learning from the sites with low alt read counts. Could these easily be dismissed if we rmdup beforehand I wonder? One thing I will try to look at today is the alt read count at sites of bonafide germline variants in this individual. We could make some argument for sensitivity and specificity based on the distribution of alt read counts at sites where the sample is a confident het or R/R homozygote. If I sent you a list of sites to pull could you make read count tables for  these, using all samples from 12WSJ?

I started with the full VCF that resulted from genotyping 520 GTEx individuals. This has 600K sites. I then extracted all of the sites where 12WSJ had coverage of at least 30X and less than 200X.

```bash
zcat GTEX-12WSJ.recode.vcf.gz | cut -f1,2 | grep -v '#' > GTEX-12WSJ.recode.pos
rm -f extract_readcounts.sh; awk '{ print "bsub -oo "$1".log \"tabix " $2 " -R GTEX-12WSJ_recode.pos | bgzip > " $1".gz\"" }'  GTEX-12WSJ_readcount_files.tsv  > extract_readcounts.sh
```
