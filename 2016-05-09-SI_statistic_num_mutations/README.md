This folder looks at the analysis of the `S_I` statistic and the
number of mutations in each sample.

#Get the number of mutations and S_I for each sample
gtexmc nmutations-sample ../2016-05-03-mpileup_all_sites_withVDB_withsampleinfo_withallTags/samtools1.1_AF0_1.vcf  ../dat/SRARunTable.txt   ../dat/Tuuli_SI/GTEx_Analysis_v6_ASE.exome.chrX_stats.zero.txt ../dat/phenotype_data/phs000424.v6.pht002742.v6.p1.c1.GTEx_Subject_Phenotypes.GRU.txt > sample_nmuts_SI.tsv
awk '{ if($1 == "sample" || $3 == 0) print }' sample_nmuts_SI.tsv   > sample_nmuts_SI_n0.tsv

##Correlation close to 0, I must be doing something terribly wrong.
> cor(SI_nmut_noNA$SI_metric, SI_nmut_noNA$total_n_mutations)
[1] -0.01335079

##Get the nzeros
#Number of samples with mutations
awk '$3 != 0' sample_nmuts_SI.tsv | wc -l
1222
awk '$3 != 0' sample_nmuts_SI.tsv | grep female | wc -l
433
awk '$3 != 0' sample_nmuts_SI.tsv | grep -w male | wc -l
788

gtexmc nmutations-sample-bcall <(zcat ../2016-08-31-whole-genome-calls-filtering-2/all_calls_sorted_filtered_11.tsv.gz)  ../dat/SRARunTable.txt   ../dat/Tuuli_SI/GTEx_Analysis_v6_ASE.exome.chrX_stats.zero.txt ../dat/phenotype_data/phs000424.v6.pht002742.v6.p1.c1.GTEx_Subject_Phenotypes.GRU.txt ../dat/flagged_samples.tsv > sample_nmuts_SI_v11.tsv
