#line264    nonsynonymous SNV          NOC2L:ENST00000327044.6:exon10:c.A1187G:p.K396R,           1          887796     887796     T          C         102         95         7          0          7          0          95         0          0          996743     469        1:887796           4.44089e-16        GTEX-X4EP          "Brain - Putamen (basal ganglia)"          0.965928           SRR1360718
import sys
def usage():
    print("python create_lolliplot.py all_calls_sorted_filtered_10.avinput.avoutput.exonic_variant_function")

hashi_samples = {} #dict with key = 1 if hash_sample


def read_hashi_samples():
    hashi_file = "../2016-05-20-hashi-analysis/hashi_thyroid_matches.csv"
    with open(hashi_file) as hfh:
        for line in hfh:
            line = line.rstrip("\n")
            fields = line.split(' ')
            sample = fields[-1]
            hashi_samples[sample] = 1

def main():
    read_hashi_samples()
    annovar = sys.argv[1]
    print("Opening " + annovar, file = sys.stderr)
    print("Hugo_Symbol\tSample_ID\tProtein_Change\tMutation_Type\tChromosome\tStart_Position\tEnd_Position\tReference_Allele\tVariant_Allele")
    with open(annovar) as annovar_fh:
        for line in annovar_fh:
            line = line.rstrip("\n")
            fields = line.split("\t")
            info = fields[2]
            info = info[0:len(info) - 1]
            gene = info.split(":")[0]
            sample = fields[-1]
            protein_change = info.split(":")[-1].upper()
            protein_change = protein_change.replace("P.", "")
            mutation_type = fields[1]
            mutation_type = mutation_type.replace(" ", "_")
            if sample in hashi_samples:
                print(gene + "\t" + sample + "\t" + protein_change + "\t" + mutation_type + "\t" + "\t".join(fields[3:8]))

main()
