set -e
rm -f  samtools1.1_output.vcf

awk '{
    sub("chr", "", $1);
    print "/gscuser/aramu/src/samtools/samtools mpileup -uv -f /gscmnt/gc2607/mardiswilsonlab/aramu/dat/hs37/all_sequences.fa /gscmnt/gc2802/halllab/cchiang/gtex/rna-seq/4101/bam/"$6".bam -r "$1":"$2"-"$2 " | grep -v \"#\" > samtools1.1_output_tmp.vcf"; \
    print  "awk '\''{ OFS=\"\\t\"; $8=$8\";sample=" $6 ";tissue=" $8 "\" ;print; }'\'' samtools1.1_output_tmp.vcf >> samtools1.1_output.vcf";
    print "rm -f samtools1.1_output_tmp.vcf"; \
}' <(cat /gscmnt/gc2607/mardiswilsonlab/aramu/test/gtex/dat/all.bscore.VAF10.out) #| bash
