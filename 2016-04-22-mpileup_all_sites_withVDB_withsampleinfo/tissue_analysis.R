require(ggplot2)
read_tissue_variant_counts <- function() {
    t<-read.table("samtools1.1_VDBgreater0_1_tissues.txt")
    colnames(t) = c("tissue")
    counts <- data.frame(table(t$tissue))
    colnames(counts) <- c("tissue", "raw_count")
    return(counts)
}

read_tissue_counts <- function() {
    tissue_counts <- read.table("tissues_samples_counts.tsv", head = T)
    return(tissue_counts)
}

plot_tissue_analysis <- function(t_vc) {
    pdf("./tissue_analysis.pdf")
    t_vc <- transform(t_vc, tissue = reorder(tissue, n_samples))
    print(ggplot(t_vc) + geom_bar(aes(x = tissue, y = n_samples), stat = "identity") +
            theme(axis.text.x = element_text(angle = 90, vjust = -0.01)) + ggtitle("Number of samples per tissue"))
    t_vc <- transform(t_vc, tissue = reorder(tissue, raw_count))
    print(ggplot(t_vc) + geom_bar(aes(x = tissue, y = raw_count), stat = "identity") +
            theme(axis.text.x = element_text(angle = 90, vjust = -0.01)) + ggtitle("Raw mutation count per tissue"))
    t_vc <- transform(t_vc, tissue = reorder(tissue, normalized_count))
    print(ggplot(t_vc) + geom_bar(aes(x = tissue, y = normalized_count), stat = "identity") +
            theme(axis.text.x = element_text(angle = 90, vjust = -0.01)) + ggtitle("Mutation count per tissue (normalized by number of samples per tissue)"))
    dev.off()
}

main <- function() {
    t_c <- read_tissue_counts()
    t_vc <- read_tissue_variant_counts()
    t_vc <- merge(t_vc, t_c, by = c("tissue"))
    t_vc$normalized_count <- t_vc$raw_count/t_vc$n_samples
    plot_tissue_analysis(t_vc)
    write.table(t_vc, "tissues_count.tsv", row.names = F, quote = F)
}

main()
