#! /usr/bin/env python

import sys
import re

vcf = sys.argv[1]

with open(vcf) as vcf_fh:
    print("\t".join(("chr", "pos", "sample", "VDB", "MQSB", "reads_on_both_strands")))
    for line in vcf_fh:
        fields = line.split("\t")
        info = fields[7]
        infos = info.split(";")
        sample = infos[-2].replace("sample=", "")
        VDB = MQSB = "NA"
        #Flag indicating variant-allele support on both strands
        robs = 0
        for anno in infos:
            if "VDB" in anno:
                VDB = anno.replace("VDB=", "")
            if "MQSB" in anno:
                MQSB = anno.replace("MQSB=", "")
            if "I16" in anno:
                i16 = anno.replace("I16=", "")
                i16_fields = i16.split(",")
                #Check for reads supporting alternate allele on both strands
                if int(i16_fields[2]) > 0 and int(i16_fields[3]) > 0:
                    robs = 1
        print("\t".join((fields[0], fields[1], sample, VDB, MQSB, str(robs))))
